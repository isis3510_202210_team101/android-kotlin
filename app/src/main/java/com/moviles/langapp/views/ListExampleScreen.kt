package com.moviles.langapp.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.compose.rememberImagePainter
import com.example.compose.LangappTheme
import com.moviles.langapp.R
import com.moviles.langapp.model.Examples
import com.moviles.langapp.viewmodels.ListExampleScreenViewModel


@Composable
fun ListScreen(
    navController: NavController,
    viewModel: ListExampleScreenViewModel = hiltViewModel()
) {
    val newsList by viewModel.getExamples().observeAsState(initial = emptyList())
    ListScreen(navController, newsList)
}

@OptIn(ExperimentalMaterial3Api::class, coil.annotation.ExperimentalCoilApi::class)
@Composable
fun ListScreen(
    navController: NavController,
    news: List<Examples>
) {
    Scaffold(
        topBar = {
            SmallTopAppBar(
                title = {
                    Text("Top news")
                }
            )
        }
    ) {
        LazyColumn {
            items(news) { new ->
                Card(
                    shape = RoundedCornerShape(8.dp),
                    modifier = Modifier
                        .padding(8.dp)
                        .fillMaxWidth()
                        .clickable {
                            TODO("Navigation Details")
                        },
                ) {
                    Column {
                        Image(
                            modifier = Modifier
                                .fillMaxWidth()
                                .aspectRatio(16f / 9f),
                            painter = rememberImagePainter(
                                data = new.urlToImage,
                                builder = {
                                    placeholder(R.drawable.placeholder)
                                    error(R.drawable.placeholder)
                                }
                            ),
                            contentDescription = null,
                            contentScale = ContentScale.FillWidth
                        )
                    }
                    Column(Modifier.padding(8.dp)) {
                        Text(new.title, fontSize = 18.sp, fontWeight = FontWeight.Bold)
                        Text(new.content ?: "", maxLines = 3)
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ListPreview() {
    LangappTheme {
        ListScreen(
            navController = rememberNavController(),
            news = arrayListOf(
                Examples(
                    "Title", "Content description", "pipe",
                    "", "https://via.placeholder.com/540x300"
                ),
                Examples(
                    "Title", "Content description", "pipe",
                    "", "https://via.placeholder.com/540x300"
                )
            )
        )
    }
}