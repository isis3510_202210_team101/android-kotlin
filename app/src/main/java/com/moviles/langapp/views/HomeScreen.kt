package com.moviles.langapp.views

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.widget.Toast
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.FloatTweenSpec
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.rememberPermissionState
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.google.android.gms.location.FusedLocationProviderClient
import com.moviles.langapp.Destinations
import com.moviles.langapp.R
import com.moviles.langapp.model.Proportions
import com.moviles.langapp.model.WordResponseDao
import com.moviles.langapp.repository.NetworkManager
import com.moviles.langapp.repository.cache.NearestWordsDataStore
import com.moviles.langapp.utils.*
import com.moviles.langapp.utils.ConnectionSingleton.observeConnectivityAsFlow
import com.moviles.langapp.viewmodels.HomeViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay


@Composable
fun NavBarHome(navController: NavController) {


    val items = listOf(
        BottomNavItem.Home,
        BottomNavItem.Dictionary,
        BottomNavItem.Cards,
        BottomNavItem.Landmarks
    )

    NavigationBar {

        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentDestination = navBackStackEntry?.destination

        items.forEachIndexed { index, item ->
            NavigationBarItem(
                icon = {
                    Icon(
                        painter = painterResource(id = item.icon),
                        contentDescription = null
                    )
                }, enabled = item.enabled,
                label = { Text(text = item.title) },
                selected = currentDestination?.hierarchy?.any { it.route == item.screen_route } == true,
                onClick = {
                    navController.navigate(item.screen_route) {
                        // Pop up to the start destination of the graph to
                        // avoid building up a large stack of destinations
                        // on the back stack as users select items
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        // Avoid multiple copies of the same destination when
                        // reselecting the same item
                        launchSingleTop = true
                        // Restore state when reselecting a previously selected item
                        restoreState = true
                    }
                }
            )
        }
    }
}

fun bandera(lang : String?):Int{
    return when (lang){
        "Spanish" -> {
            R.drawable.spanish
        }
        "German" -> {
            R.drawable.german
        }
        "Italian" -> {
            R.drawable.italian
        }
        "Portuguese" -> {
            R.drawable.portuguese
        }
        else ->{
            R.drawable.english
        }
    }
}

fun emptyProp(prop: Proportions):Boolean {
    var ans:Boolean = false
    if (prop.Spanish == null && prop.Portuguese == null && prop.Italian == null &&
            prop.German == null && prop.English == null){
        ans = true
    }
    return ans
}

@Composable
fun TopBarHome(navController: NavController, onSignOut: () -> Unit) {
    val context = LangappContext.getContext()
    val prefs = SharedPref.getInstance(context)
    val contextoLocal = LocalContext.current

    SmallTopAppBar(
        title = {
            Text(
                "LangApp",
                color = MaterialTheme.colorScheme.primary,
                fontWeight = FontWeight.Bold
            )
        },
        actions = {
            IconButton(onClick = {
                if(NetworkManager.checkForInternet(contextoLocal)){
                    navController.navigate(Destinations.SELECT_LEARN_LANG)
                }
                else {
                    Toast.makeText(contextoLocal,"Please check your internet connection", Toast.LENGTH_SHORT).show()
                }
            }) {
                Image(
                    painter = painterResource(bandera(prefs.getVal("LEARNINGLANGUAGE"))),
                    contentDescription = "Localized description",
                )
            }
            IconButton(onClick = {
                onSignOut()
                navController.navigate(Destinations.LOGIN_SCREEN)
            }) {
                Icon(
                    imageVector = Icons.Filled.Settings,
                    contentDescription = "Localized description",
                    tint = MaterialTheme.colorScheme.primary
                )
            }
        }
    )
}

@OptIn(
    ExperimentalMaterial3Api::class,
    ExperimentalPermissionsApi::class
)
@Composable
fun HomeScreen(navController: NavController, nearestWords: List<WordResponseDao>,
               locationPermissionState: PermissionState?,
               langProps: Proportions, forgotten: List<String>,
               popularLandmark: String, onSignOut: ()-> Unit) {

    val systemUiController = rememberSystemUiController()
    systemUiController.setSystemBarsColor(
        MaterialTheme.colorScheme.surface
    )

    val snackbarHostState = remember { SnackbarHostState() }


    Scaffold(
        topBar = { TopBarHome(navController, onSignOut) },
        bottomBar = { NavBarHome(navController) },
        snackbarHost = { SnackbarHost(snackbarHostState) }

    ) {
        Column(modifier = Modifier
            .verticalScroll(rememberScrollState())
            .padding(it)) {
            val context = LocalContext.current
            ConnectivityStatus()
            FeedComponent(nearestWords, locationPermissionState)
            Spacer(modifier = Modifier.padding(top = 5.dp))
            if(NetworkManager.checkForInternet(context) && !emptyProp(langProps))Piechart(langProps)
            Spacer(modifier = Modifier.padding(top = 5.dp))
            if(NetworkManager.checkForInternet(context) && forgotten.isNotEmpty()) ForgotenLanguages(forgotten = forgotten)
            Spacer(modifier = Modifier.padding(top = 5.dp))
            if(NetworkManager.checkForInternet(context)) PopularLandmark(popular = popularLandmark)
            Spacer(modifier = Modifier.padding(top = 25.dp))
        }
    }
}


@SuppressLint("MissingPermission")
@OptIn(
    ExperimentalPermissionsApi::class,
    ExperimentalCoroutinesApi::class
)
@Composable
fun HomeScreenStart(
    navController: NavController,
    fusedLocationProviderClient: FusedLocationProviderClient,
    dataStore: NearestWordsDataStore,
    viewModel: HomeViewModel = hiltViewModel()
) {
    LockScreenOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
    viewModel.checkPreferences()
    val connection by connectivityState()
    val isConnected = connection === ConnectionState.Available
    val current = LocalContext.current
    val locationPermissionState =
        rememberPermissionState(android.Manifest.permission.ACCESS_COARSE_LOCATION)
    val props by viewModel._proportions.observeAsState(initial = Proportions())
    viewModel.getProportions(current)

    val forgotten by viewModel._forgotten.observeAsState(initial = emptyList<String>())
    viewModel.getForgotten(current)

    val popularLandmark by viewModel._pop.observeAsState(initial = String())
    viewModel.getPopular(current)

    val dataStore = NearestWordsDataStore(current)

    when (locationPermissionState.status) {

        PermissionStatus.Granted -> {
            val wordsList by viewModel._words.observeAsState(initial = emptyList())
            fusedLocationProviderClient.lastLocation.addOnSuccessListener {
                if (it != null) {
                    viewModel.getNearestWords(it.latitude, it.longitude, isConnected, dataStore)
                }
            }
            HomeScreen(navController = navController, nearestWords = wordsList, locationPermissionState, langProps = props, forgotten = forgotten,popularLandmark = popularLandmark, onSignOut = viewModel::logOut)
        }
        is PermissionStatus.Denied -> {

            HomeScreen(navController = navController, emptyList(), locationPermissionState, langProps = props, forgotten = forgotten, popularLandmark = popularLandmark, onSignOut = viewModel::logOut)
        }

    }


}


@OptIn(
    ExperimentalMaterial3Api::class,
    ExperimentalPermissionsApi::class
)
@Composable
fun FeedComponent(nearestWords: List<WordResponseDao>, locationPermissionState: PermissionState?) {
    Column(modifier = Modifier.padding(25.dp)) {
        Card {
            Column(
                Modifier
                    .fillMaxWidth()
                    .aspectRatio(16f / 13f)
                    .padding(25.dp, 10.dp)
            ) {
                Text(
                    text = "Last time you were here...",
                    style = MaterialTheme.typography.headlineMedium
                )
                Spacer(modifier = Modifier.padding(top = 10.dp))

                if (locationPermissionState?.status is PermissionStatus.Denied) {
                    Button(
                        { locationPermissionState.launchPermissionRequest() },
                        modifier = Modifier.align(Alignment.CenterHorizontally)
                    ) {
                        Text("Give location access")
                    }
                } else if (nearestWords.isEmpty()) {
                    Text("It looks like you did not learn words nearby")

                } else if (nearestWords.size == 2) {
                    Text(
                        nearestWords[0].word,
                        style = MaterialTheme.typography.titleLarge,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        nearestWords[1].word,
                        style = MaterialTheme.typography.titleLarge,
                        fontWeight = FontWeight.Bold
                    )
                } else if (nearestWords.size == 1) {
                    Text(
                        nearestWords[0].word,
                        style = MaterialTheme.typography.titleLarge,
                        fontWeight = FontWeight.Bold
                    )
                } else {
                    Text(
                        nearestWords[0].word,
                        style = MaterialTheme.typography.titleLarge,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        nearestWords[1].word,
                        style = MaterialTheme.typography.titleLarge,
                        fontWeight = FontWeight.Bold
                    )
                    val count = nearestWords.size - 2
                    Text("+$count more words", style = MaterialTheme.typography.titleLarge)

                }
                Spacer(modifier = Modifier.padding(top = 10.dp))
                Button(onClick = { }, modifier = Modifier.align(Alignment.CenterHorizontally)) {
                    Text(text = "Start Flashcard Session")
                }


            }
        }
    }
}


@OptIn(
    ExperimentalMaterial3Api::class,
    ExperimentalPermissionsApi::class
)
@Composable
fun ForgotenLanguages(forgotten: List<String>) {
    Column(modifier = Modifier.padding(25.dp)) {
        Card {
            Column(
                Modifier
                    .fillMaxWidth()
                    .aspectRatio(16f / 13f)
                    .padding(25.dp, 10.dp)
            ) {
                Text(
                    text = "Hey! it's been a long time since you practiced these languages",
                    style = MaterialTheme.typography.headlineMedium
                )
                Spacer(modifier = Modifier.padding(top = 10.dp))
                if (forgotten.size == 2) {
                    Text(
                        forgotten[0],
                        style = MaterialTheme.typography.titleLarge,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        forgotten[1],
                        style = MaterialTheme.typography.titleLarge,
                        fontWeight = FontWeight.Bold
                    )
                } else if (forgotten.size == 1) {
                    Text(
                        forgotten[0],
                        style = MaterialTheme.typography.titleLarge,
                        fontWeight = FontWeight.Bold
                    )
                } else {
                    Text(
                        forgotten[0],
                        style = MaterialTheme.typography.titleLarge,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        forgotten[1],
                        style = MaterialTheme.typography.titleLarge,
                        fontWeight = FontWeight.Bold
                    )
                    val count = forgotten.size - 2
                    Text("+$count more words", style = MaterialTheme.typography.titleLarge)

                }
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class,
    com.google.accompanist.permissions.ExperimentalPermissionsApi::class
)
@Composable
fun PopularLandmark(popular: String) {
    Column(modifier = Modifier.padding(25.dp)) {
        Card() {
            Column(
                Modifier
                    .fillMaxWidth()
                    .aspectRatio(16f / 13f)
                    .padding(25.dp, 10.dp)) {
                Text(
                    text = "The most popular landmark in LangApp is ...",
                    style = MaterialTheme.typography.headlineMedium
                )
                Spacer(modifier = Modifier.padding(top = 10.dp))
                Text(popular, style = MaterialTheme.typography.titleLarge, fontWeight = FontWeight.Bold)
            }
        }
    }
}

@ExperimentalCoroutinesApi
@Composable
fun connectivityState(): State<ConnectionState> {
    val context = LocalContext.current

    // Creates a State<ConnectionState> with current connectivity state as initial value
    return produceState(initialValue = context.currentConnectivityState) {
        // In a coroutine, can make suspend calls
        context.observeConnectivityAsFlow().collect { value = it }
    }
}

@OptIn(ExperimentalCoroutinesApi::class)
@Composable
fun ConnectivityStatus() {
    val connection by connectivityState()

    val isConnected = connection === ConnectionState.Available
    var visibility by remember { mutableStateOf(false) }

    AnimatedVisibility(
        visible = visibility,
        enter = expandVertically(),
        exit = shrinkVertically()
    ) {
        ConnectivityStatusBox(isConnected = isConnected)
    }

    LaunchedEffect(isConnected) {
        visibility = if (!isConnected) {
            true
        } else {
            delay(2000)
            false
        }
    }
}

@Composable
fun ConnectivityStatusBox(isConnected: Boolean) {
    val backgroundColor by animateColorAsState(if (isConnected) Color.Green else Color.Red)
    val message = if (isConnected) "Back Online!" else "No Internet Connection!"
    val iconResource = if (isConnected) {
        R.drawable.ic_baseline_cloud_done_24
    } else {
        R.drawable.ic_baseline_cloud_off_24
    }

    Box(
        modifier = Modifier
            .background(backgroundColor)
            .fillMaxWidth()
            .padding(8.dp),
        contentAlignment = Alignment.Center
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Icon(painterResource(id = iconResource), "Connectivity Icon", tint = Color.White)
            Spacer(modifier = Modifier.size(8.dp))
            Text(message, color = Color.White, fontSize = 15.sp)
        }
    }
}

@OptIn(
    ExperimentalMaterial3Api::class,
    ExperimentalPermissionsApi::class
)
@Composable
fun Piechart(prop: Proportions) {
    Column(modifier = Modifier.padding(25.dp)) {
        Card {
            Column(
                Modifier
                    .align(Alignment.End)
                    .fillMaxWidth()
                    .aspectRatio(16f / 13f)
                    .padding(25.dp, 10.dp)
            ) {
                Text(
                    text = "How are you adding new words?",
                    style = MaterialTheme.typography.headlineMedium
                )
                Row(
                    modifier = Modifier
                        .padding(start = 15.dp)
                ) {
                    Column {
                        Spacer(modifier = Modifier.padding(top = 20.dp))
                        Row {
                            Canvas(
                                modifier = Modifier
                                    .width(15.dp)
                                    .padding(top = 7.dp)
                            ) {
                                drawCircle(
                                    color = Color(240, 192, 72),
                                    radius = 12f,
                                    center = Offset(x = 5f, y = 10f)
                                )
                            }
                            Text("English")
                        }
                        Spacer(modifier = Modifier.padding(top = 3.dp))
                        Row {
                            Canvas(
                                modifier = Modifier
                                    .width(15.dp)
                                    .padding(top = 7.dp)
                            ) {
                                drawCircle(
                                    color = Color(0, 26, 66),
                                    radius = 12f,
                                    center = Offset(x = 5f, y = 10f)
                                )
                            }
                            Text("German")
                        }
                        Spacer(modifier = Modifier.padding(top = 3.dp))
                        Row {
                            Canvas(
                                modifier = Modifier
                                    .width(15.dp)
                                    .padding(top = 7.dp)
                            ) {
                                drawCircle(
                                    color = Color(10, 69, 142),
                                    radius = 12f,
                                    center = Offset(x = 5f, y = 10f)
                                )
                            }
                            Text("Italian")
                        }
                        Spacer(modifier = Modifier.padding(top = 3.dp))
                        Row {
                            Canvas(
                                modifier = Modifier
                                    .width(15.dp)
                                    .padding(top = 7.dp)
                            ) {
                                drawCircle(
                                    color = Color(75, 118, 194),
                                    radius = 12f,
                                    center = Offset(x = 5f, y = 0f)
                                )
                            }
                            Text("Portuguese")
                        }
                        Spacer(modifier = Modifier.padding(top = 3.dp))
                        Row {
                            Canvas(
                                modifier = Modifier
                                    .width(15.dp)
                                    .padding(top = 7.dp)
                            ) {
                                drawCircle(
                                    color = Color(129, 171, 251),
                                    radius = 12f,
                                    center = Offset(x = 5f, y = 10f)
                                )
                            }
                            Text("Spanish")
                        }
                    }
                    val engl = if (prop.English != null) {
                        prop.English.toFloat()
                    } else {
                        0f
                    }
                    val germ = if (prop.German != null) {
                        prop.German.toFloat()
                    } else {
                        0f
                    }
                    val ital = if (prop.Italian != null) {
                        prop.Italian.toFloat()
                    } else {
                        0f
                    }
                    val port = if (prop.Portuguese != null) {
                        prop.Portuguese.toFloat()
                    } else {
                        0f
                    }
                    val spa = if (prop.Spanish != null) {
                        prop.Spanish.toFloat()
                    } else {
                        0f
                    }

                    val point = listOf(engl, germ, ital, port, spa)
                    val color = listOf(
                        Color(240, 192, 72),
                        Color(0, 26, 66),
                        Color(10, 69, 142),
                        Color(75, 118, 194),
                        Color(129, 171, 251)
                    )
                    val sum = point.sum()
                    var startAngle = 0f
                    val radius = 220f
                    val rect = Rect(Offset(-radius, -radius), Size(2 * radius, 2 * radius))
                    val path = Path()
                    val angles = mutableListOf<Float>()
                    var start by remember { mutableStateOf(false) }
                    val sweepPre by animateFloatAsState(
                        targetValue = if (start) 1f else 0f,
                        animationSpec = FloatTweenSpec(duration = 1000)
                    )
                    Canvas(
                        modifier = Modifier
                            .fillMaxWidth()
                            .aspectRatio(16f / 13f)
                            .padding(start = 18.dp, bottom = 10.dp)
                    ) {
                        translate(radius, radius) {
                            start = true
                            for ((i, p) in point.withIndex()) {
                                val sweepAngle = p / sum * 360f
                                path.moveTo(0f, 0f)
                                if(p == sum){
                                    path.arcTo(rect = rect, startAngle, sweepAngle * sweepPre - 0.001f, false)
                                }else{
                                    path.arcTo(rect = rect, startAngle, sweepAngle * sweepPre, false)
                                }
                                angles.add(sweepAngle)
                                drawPath(path = path, color = color[i])
                                path.reset()
                                startAngle += sweepAngle
                            }
                        }
                    }
                }
            }
        }
    }
}