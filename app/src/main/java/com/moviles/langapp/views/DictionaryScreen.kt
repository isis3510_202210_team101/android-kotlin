package com.moviles.langapp.views

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.compose.animation.*
import androidx.compose.animation.core.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.paging.LoadState
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemsIndexed
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import com.example.compose.md_theme_light_onSurface
import com.moviles.langapp.Destinations
import com.moviles.langapp.R
import com.moviles.langapp.model.DetailWord
import com.moviles.langapp.model.DictionaryWord
import com.moviles.langapp.repository.NetworkManager
import com.moviles.langapp.utils.Constants.CollapseAnimation
import com.moviles.langapp.utils.Constants.ExpandAnimation
import com.moviles.langapp.utils.Constants.FadeInAnimation
import com.moviles.langapp.utils.Constants.FadeOutAnimation
import com.moviles.langapp.utils.LangappContext
import com.moviles.langapp.utils.OnboardingCache
import com.moviles.langapp.utils.SharedPref
import com.moviles.langapp.viewmodels.DictionaryViewModel


@OptIn(ExperimentalMaterial3Api::class, ExperimentalAnimationApi::class)
@Composable
fun DictionaryScreen(
    navController: NavController, viewModel: DictionaryViewModel
) {
    val context = LocalContext.current
    val cards = viewModel.flow.collectAsLazyPagingItems()
    NotificationMessageDictionary(viewModel)
    Scaffold(
        topBar = { TopBarHomeDict(navController) },
        bottomBar = { NavBarHome(navController) },
        floatingActionButton = { FAB(navController) }
    ) {
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            contentPadding = it
        ) {
            if (cards.loadState.refresh == LoadState.Loading) {
                item {
                    Text(
                        text = "Waiting for items to load",
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentWidth(Alignment.CenterHorizontally)
                    )
                    CircularProgressIndicator(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentWidth(Alignment.CenterHorizontally)
                    )
                }
            }
            itemsIndexed(cards) { _, card ->
                var expanded by rememberSaveable {
                    mutableStateOf(false)
                }
                var isLoading by rememberSaveable {
                    mutableStateOf(false)
                }

                ExpandableCard(
                    card = card,
                    onClick = {
                        if (!expanded) {
                            isLoading = true
                            viewModel.fetchWordDetail(
                                card,
                                card?.word?.id ?: "",
                                NetworkManager.checkForInternet(context),
                                { expanded = !expanded },
                                { isLoading = !isLoading })
                        } else {
                            expanded = !expanded
                            isLoading = false
                        }
                    },
                    expanded = expanded,
                    isLoading = isLoading
                )
            }
            if (cards.loadState.append == LoadState.Loading) {
                item {
                    CircularProgressIndicator(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentWidth(Alignment.CenterHorizontally)
                    )
                }
            }
        }
    }
}


@Composable
fun FAB(navController: NavController) {
    val cm =
        LocalContext.current.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val cl = LocalContext.current
    FloatingActionButton(
        onClick = {
            @Suppress("DEPRECATION") val isConnected: Boolean =
                cm.activeNetworkInfo?.isConnectedOrConnecting == true
            if (!isConnected) {
                Toast.makeText(
                    cl,
                    "No connection. You need connection to add a word. Please try again latter.",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                navController.navigate(Destinations.ADD_NEW_WORD)
            }
        }) {
        Icon(
            painter = painterResource(R.drawable.ic_round_add_24),
            contentDescription = null
        )
    }
}

@Composable
fun TopBarHomeDict(navController: NavController) {
    val context = LangappContext.getContext()
    val prefs = SharedPref.getInstance(context)
    val contextoLocal = LocalContext.current

    SmallTopAppBar(
        title = {
            Text(
                "My Dictionary",
                color = MaterialTheme.colorScheme.primary,
                style = MaterialTheme.typography.headlineSmall
            )
        },
        actions = {
            IconButton(onClick = {
                if(NetworkManager.checkForInternet(contextoLocal)){
                    navController.navigate(Destinations.SELECT_LEARN_LANG)
                }
                else {
                    Toast.makeText(contextoLocal,"Please check your internet connection",Toast.LENGTH_SHORT).show()
                }

            }) {
                Image(
                    painter = painterResource(bandera(prefs.getVal("LEARNINGLANGUAGE"))),
                    contentDescription = "Localized description"
                )
            }
            IconButton(onClick = { /* doSomething() */ }) {
                Icon(
                    imageVector = Icons.Filled.Search,
                    contentDescription = "Localized description",
                    tint = MaterialTheme.colorScheme.primary
                )
            }
        }
    )
}

@OptIn(ExperimentalMaterialApi::class, coil.annotation.ExperimentalCoilApi::class)
@ExperimentalAnimationApi
@SuppressLint("UnusedTransitionTargetStateParameter")
@Composable

//falta agregar el translation en el not expanded card
fun ExpandableCard(
    card: DictionaryWord?,
    onClick: () -> Unit,
    expanded: Boolean,
    isLoading: Boolean
) {
    if (card == null) {
        return
    }
    val interactionSource = remember { MutableInteractionSource() }
    val transitionState = remember {
        MutableTransitionState(expanded).apply {
            targetState = !expanded
        }
    }
    val transition = updateTransition(targetState = transitionState, label = "transition")
    val cardPaddingHorizontal by transition.animateDp({
        tween(durationMillis = ExpandAnimation)
    }, label = "paddingTransition") {
        20.dp
    }
    val cardElevation by transition.animateDp({
        tween(durationMillis = ExpandAnimation)
    }, label = "elevationTransition") {
        if (expanded) 20.dp else 5.dp
    }
    val cardRoundedCorners by transition.animateDp({
        tween(
            durationMillis = ExpandAnimation,
            easing = FastOutSlowInEasing
        )
    }, label = "cornersTransition") {
        15.dp
    }

    Card(
        backgroundColor = Color.LightGray,
        elevation = cardElevation,
        shape = RoundedCornerShape(cardRoundedCorners),
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = cardPaddingHorizontal,
                vertical = 8.dp
            )
            .clickable(
                interactionSource = interactionSource,
                indication = null
            ) {
                onClick()
            }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Box {
                Row(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Image(
                        modifier = Modifier
                            .size(60.dp)
                            .clip(RoundedCornerShape(topStart = 16.dp, bottomStart = 16.dp)),
                        painter = rememberImagePainter(
                            data = card.thumbnail,
                            builder = {
                                placeholder(R.drawable.placeholder)
                                error(R.drawable.placeholder)
                            }
                        ),
                        contentDescription = null,
                        contentScale = ContentScale.Crop
                    )

                    Column(
                        modifier = Modifier
                            .weight(0.85f)
                            .align(Alignment.CenterVertically)

                    ) {
                        Text(
                            text = card.customTranslation,
                            color = md_theme_light_onSurface,
                            textAlign = TextAlign.Start,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(12.dp)
                        )
                    }
                    if (isLoading) {
                        Spacer(
                            Modifier
                                .weight(1f)
                                .fillMaxHeight()
                                .background(Color.Green)
                        )
                        CircularProgressIndicator(modifier = Modifier.padding(0.dp, 5.dp))
                    }

                }
            }
            ExpandableContent(expanded, card) //que se pasa, bucar la cardDetail asociada al word,
            //o una función directa desde el viewmodel?
            //cambiar de card tipo word al detail tipo cardDetail
        }
    }
}

@OptIn(ExperimentalCoilApi::class)
@ExperimentalAnimationApi
@Composable
fun ExpandableContent(expanded: Boolean = true, originalCard: DictionaryWord?) {

    val enterFadeIn = remember {
        fadeIn(
            animationSpec = TweenSpec(
                durationMillis = FadeInAnimation,
                easing = FastOutLinearInEasing
            )
        )
    }
    val enterExpand = remember {
        expandVertically(animationSpec = tween(ExpandAnimation))
    }
    val exitFadeOut = remember {
        fadeOut(
            animationSpec = TweenSpec(
                durationMillis = FadeOutAnimation,
                easing = LinearOutSlowInEasing
            )
        )
    }
    val exitCollapse = remember {
        shrinkVertically(animationSpec = tween(CollapseAnimation))
    }

    val card = originalCard?.detailWord ?: DetailWord()

    AnimatedVisibility(
        visible = expanded,
        enter = enterExpand + enterFadeIn,
        exit = exitCollapse + exitFadeOut
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color(217, 246, 255))
                .padding(8.dp)
        ) {
            Text(
                //usar nuevo objeto de detail
                text = card.translation,
                textAlign = TextAlign.Left,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
                color = md_theme_light_onSurface,
                style = MaterialTheme.typography.bodyLarge
            )
            Text(
                //usar llamado a VM con función que retorne el detail
                text = originalCard?.word?.id ?: "",
                textAlign = TextAlign.Left,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
                color = md_theme_light_onSurface,
                style = MaterialTheme.typography.bodyMedium

            )
            Image(
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(10f / 3f),
                painter = rememberImagePainter(
                    //usar llamado a VM con función que retorne el detail
                    data = card.photo,
                    builder = {
                        placeholder(R.drawable.placeholder)
                        error(R.drawable.placeholder)
                    }
                ),
                contentDescription = null,
                contentScale = ContentScale.FillWidth
            )
            Text(
                //usar la descripción que está en translationsDescription en db
                //usar llamado a VM con función que retorne el detail
                text = card.description,
                textAlign = TextAlign.Left,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
                color = md_theme_light_onSurface
            )

            Row(
                horizontalArrangement = Arrangement.End
            ) {

                androidx.compose.material3.OutlinedButton(
                    onClick = { },
                    modifier = Modifier.size(64.dp, 40.dp)
                ) {
                    androidx.compose.material3.Icon(
                        painter = painterResource(R.drawable.ic_volume_24),
                        contentDescription = null
                    )
                }
                Spacer(modifier = Modifier.padding(start = 30.dp))
                FilledTonalButton(
                    onClick = { },
                    modifier = Modifier.size(175.dp, 35.dp)
                ) {
                    androidx.compose.material3.Text(text = "Add to flashcard")
                }
            }
        }
    }
}