package com.moviles.langapp.views

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.widget.Toast
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.compose.md_theme_light_primary
import com.moviles.langapp.Destinations
import com.moviles.langapp.model.Languages
import com.moviles.langapp.viewmodels.OnboardingVM

var preferredLanguageUserGlobal = "English"
var dbDefault = "English"
var nativeLanguageUserGlobal = "Spanish"

@Composable
fun OnboardingScreen(
    navController: NavController,
    viewModel: OnboardingVM = hiltViewModel()
) {
    val languaList by viewModel.getLanguages1().observeAsState(initial = emptyList())
    //OnboardingScreen(navController, languaList, viewModel::setLearningLanguages)
    OnboardingScreen(navController, languaList, viewModel::createUser)
}

@OptIn(ExperimentalMaterial3Api::class, coil.annotation.ExperimentalCoilApi::class)
@Composable
fun OnboardingScreen(
    navController: NavController,
    lanList: List<Languages>,
    //setLearningLanguages: (List<Languages>) -> Unit
    createUser: (List<Languages>) -> Unit
) {
    fun checkForInternet(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }

    val context = LocalContext.current
    val interactionSource = remember { MutableInteractionSource() }

    @Composable
    fun TopBarHomeDict() {

        SmallTopAppBar(
            title = {
                Text(
                    "You want to learn...",
                    color = MaterialTheme.colorScheme.primary,
                    style = MaterialTheme.typography.headlineSmall
                )
            },
            actions = {
                IconButton(onClick = { /* doSomething() */ }) {
                    Icon(
                        imageVector = Icons.Filled.Search,
                        contentDescription = "Localize language",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }
            }
        )
    }

    val selectedItems = rememberSaveable {
        mutableStateOf(setOf<Int>())
    }

    @Composable
    fun FAB(navController: NavController) {
        val context = LocalContext.current
        FloatingActionButton(
            onClick = {
                if (checkForInternet(context)) {
                    if (selectedItems.value.isNotEmpty()) {
                        createUser(selectedItems.value.map { lanList[it] })
                        navController.navigate(Destinations.HOME_SCREEN)
                    } else {
                        Toast.makeText(
                            context,
                            "Please select one or more languages",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    Toast.makeText(
                        context,
                        "You don't have internet connection",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }) {
            Icon(
                imageVector = Icons.Filled.Check,
                contentDescription = "Define languages to learn",
                tint = MaterialTheme.colorScheme.primary
            )
        }
    }



    Scaffold(
        topBar = { TopBarHomeDict() },
        floatingActionButton = { FAB(navController) }
    ) {
        LazyColumn {
            items(lanList.size) { index ->
                Card(
                    backgroundColor = Color.LightGray,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(10.dp),
                    elevation = 10.dp,
                    shape = RoundedCornerShape(10.dp)
                ) {
                    Row(Modifier.fillMaxWidth()) {
                        Text(
                            text = lanList[index].language,
                            style = MaterialTheme.typography.titleLarge,
                            modifier = Modifier
                                .padding(start = 20.dp, top = 10.dp)
                        )
                        /*Text(
                            text = selectedItems.toString(),
                            color = Color.Black,
                            fontWeight = FontWeight.Bold,
                            fontSize = 10.sp,
                        )*/
                        val checked = selectedItems.value.contains(index)
                        Column(
                            modifier = Modifier
                                .fillMaxWidth(),
                            horizontalAlignment = Alignment.End
                        ) {
                            Checkbox(
                                checked = checked,
                                onCheckedChange = {
                                    print(selectedItems)
                                    selectedItems.value = if (checked) {
                                        selectedItems.value - index
                                    } else {
                                        selectedItems.value + index
                                    }
                                },
                                colors = CheckboxDefaults.colors(md_theme_light_primary),
                            )
                        }
                    }
                }
            }
        }
    }
}






