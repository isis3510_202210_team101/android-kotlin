package com.moviles.langapp.views

import com.moviles.langapp.R

sealed class BottomNavItem(
    var title: String,
    var icon: Int,
    var screen_route: String,
    var enabled: Boolean = true
) {
    object Home : BottomNavItem("Home", R.drawable.ic_baseline_home_24, "HOME_SCREEN")
    object Dictionary :
        BottomNavItem(
            "Dictionary",
            R.drawable.ic_outline_collections_bookmark_24,
            "DICTIONARY_SCREEN"
        )

    object Cards : BottomNavItem("Cards", R.drawable.ic_outline_library_books_24, "cards", false)
    object Landmarks : BottomNavItem("Landmarks", R.drawable.ic_outline_place_24, "LANDMARKS_SCREEN")
}
