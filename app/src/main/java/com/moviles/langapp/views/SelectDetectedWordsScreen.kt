package com.moviles.langapp.views

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.compose.LangappTheme
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.moviles.langapp.Destinations
import com.moviles.langapp.model.PredictionsDao
import com.moviles.langapp.utils.AddWordCache
import com.moviles.langapp.utils.LangappContext
import com.moviles.langapp.utils.SharedPref
import com.moviles.langapp.viewmodels.ObjectRecognitionViewModel

var termino = true

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SelectDetectedWordsScreen(
    navController: NavController,
    viewModel: ObjectRecognitionViewModel = hiltViewModel()
) {
    val context = LangappContext.getContext()
    val prefs = SharedPref.getInstance(context)
    var native = prefs.getVal("NATIVE")?:"English"
    var native_abv = "en"
    when (native) {
        "English" -> {
            native_abv = "en"
        }
        "German" -> {
            native_abv = "de"
        }
        "Italian" -> {
            native_abv = "it"
        }
        "Portuguese" -> {
            native_abv = "pt"
        }
        "Spanish" -> {
            native_abv = "es"
        }
    }
    val labels by viewModel._predictions.observeAsState(initial = emptyList())
    viewModel.getImageLabels(
        AddWordCache.instance.get_from_cache("path_post"),
        native_abv,
        AddWordCache.instance.get_from_cache("target_post")
    )

    Scaffold(
        topBar = { TopBarAddWord(navController) },
        bottomBar = { NavBarHome(navController) }

    ) {
        DetectedWordsComponent(navController = navController, detectedObjects = labels, viewModel)
    }
}

@Composable
fun DetectedWordsComponent(
    navController: NavController,
    detectedObjects: List<PredictionsDao>,
    viewModel: ObjectRecognitionViewModel
) {
    val context = LocalContext.current
    val cm =
        LocalContext.current.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    Column(
        Modifier
            .fillMaxSize()
    ) {
        LazyColumn {
            itemsIndexed(detectedObjects) { index, item ->
                Card(
                    backgroundColor = MaterialTheme.colorScheme.primaryContainer.copy(0.5f),
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable {
                            @Suppress("DEPRECATION") val isConnected: Boolean =
                                cm.activeNetworkInfo?.isConnectedOrConnecting == true
                            if (!isConnected) {
                                Toast
                                    .makeText(
                                        context,
                                        "No connection. You need connection to add a word. Please try again latter.",
                                        Toast.LENGTH_LONG
                                    )
                                    .show()
                            } else {
                                if (item.objective == "None of the above") {
                                    AddWordCache.instance.save_in_cache("selected_Word", "")
                                    navController.navigate(Destinations.ADD_NEW_WORD)
                                } else {
                                    AddWordCache.instance.save_in_cache(
                                        "selected_Word",
                                        item.objective
                                    )
                                    navController.navigate(Destinations.ADD_DETECTED_WORD)
                                }
                            }
                        }
                        .size(90.dp, 60.dp)
                        .padding(start = 10.dp, end = 10.dp, top = 5.dp, bottom = 5.dp)
                ) {
                    Text(
                        modifier = Modifier.padding(start = 5.dp, top = 5.dp),
                        text = item.objective,
                        style = MaterialTheme.typography.bodyLarge,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        modifier = Modifier.padding(top = 25.dp, start = 15.dp),
                        text = item.native, style = MaterialTheme.typography.bodySmall
                    )
                }
            }

        }

    }
    if (detectedObjects.size == 1 && termino) {
        termino = false
        AddWordCache.instance.clear_cache()
        Toast.makeText(
            LocalContext.current,
            "No object detected. Please take another photo",
            Toast.LENGTH_LONG
        ).show()
        navController.navigate(Destinations.ADD_NEW_WORD)
    }
}

@OptIn(ExperimentalPermissionsApi::class)
@Preview
@Composable
fun DetectedWordsComponentPreview() {
    LangappTheme {
        SelectDetectedWordsScreen(navController = rememberNavController())
    }
}
