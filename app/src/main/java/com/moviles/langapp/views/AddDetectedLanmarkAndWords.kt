package com.moviles.langapp.views

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import android.graphics.drawable.Icon
import androidx.compose.runtime.getValue
import androidx.compose.ui.unit.sp
import android.net.ConnectivityManager
import android.util.Log
import com.moviles.langapp.utils.LangappContext
import android.widget.Toast
import androidx.compose.runtime.MutableState
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.*
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import com.moviles.langapp.viewmodels.AddLandmarkScreenViewModel
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.moviles.langapp.model.LandmarksDao
import com.moviles.langapp.model.PredictionsDao
import com.moviles.langapp.utils.AddLandmarkCache
import com.moviles.langapp.utils.AddWordCache
import com.moviles.langapp.viewmodels.LandmarkDetectionViewModel
import com.moviles.langapp.viewmodels.ObjectRecognitionViewModel
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Card
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import com.example.compose.md_theme_light_primary
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.moviles.langapp.Destinations
import com.moviles.langapp.R
import com.moviles.langapp.utils.SharedPref
import com.moviles.langapp.viewmodels.AddWordScreenViewModel

var termino_Land = false
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddDetectedLanmarkAndWordsScreen(
    navController: NavController,
    viewModelLandmark:LandmarkDetectionViewModel = hiltViewModel(),
    viewModelObjects: ObjectRecognitionViewModel = hiltViewModel(),
    viewModelAddWord: AddWordScreenViewModel = hiltViewModel()
){
    val selectedWords = rememberSaveable {
        mutableStateOf(setOf<Int>())
    }
    val landmarks by viewModelLandmark._predictions.observeAsState(initial = emptyList())
    val labels by viewModelObjects._predictions.observeAsState(initial = emptyList())
    val context = LangappContext.getContext()
    val prefs = SharedPref.getInstance(context)
    val learning = prefs.getVal("LEARNINGLANGUAGE")?:"Spanish"
    var learning_abv ="es"
    when (learning) {
        "English" -> {
            learning_abv = "en"
        }
        "German" -> {
            learning_abv = "de"
        }
        "Italian" -> {
            learning_abv = "it"
        }
        "Portuguese" -> {
            learning_abv = "pt"
        }
        "Spanish" -> {
            learning_abv = "es"
        }
    }
    var native = prefs.getVal("NATIVE")?:"English"
    var native_abv = "en"
    when (native) {
        "English" -> {
            native_abv = "en"
        }
        "German" -> {
            native_abv = "de"
        }
        "Italian" -> {
            native_abv = "it"
        }
        "Portuguese" -> {
            native_abv = "pt"
        }
        "Spanish" -> {
            native_abv = "es"
        }
    }
    viewModelLandmark.getLandmark(AddLandmarkCache.instance.get_from_cache("path_post"))
    viewModelObjects.getImageLabels(AddLandmarkCache.instance.get_from_cache("path_post"),
        native_abv,
        learning_abv
    )

    Scaffold(
        topBar = {TopBarAddLandmarkAndWords(navController)},
        bottomBar = {NavBarHome(navController)},
        floatingActionButton = {FABAddLandmark(navController, viewModel=viewModelAddWord, selectedWords=selectedWords, labels = labels, landmarks = landmarks)}
    ) {
        AddDetectedLandmarkComponent(navController = navController,
            detectedLandmark = landmarks,
            viewModelLandmark = viewModelLandmark,
            detectedObjects = labels,
            viewModelObjects = viewModelObjects,
            selectedWords = selectedWords
        )
    }
}

@Composable
fun FABAddLandmark(navController: NavController, viewModel: AddWordScreenViewModel, selectedWords:MutableState<Set<Int>>,
                    labels:List<PredictionsDao>, landmarks:List<LandmarksDao>){
    val cm = LocalContext.current.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val cl = LocalContext.current
    FloatingActionButton(
        onClick = {
            @Suppress("DEPRECATION") val isConnected:Boolean = cm.activeNetworkInfo?.isConnectedOrConnecting== true
            if (!isConnected){
                Toast.makeText(cl, "No connection. Please try again latter.", Toast.LENGTH_LONG).show()
            }else{
                if (selectedWords.value.isNotEmpty()){
                    val listWords2Add = selectedWords.value.map {labels[it]}
                    listWords2Add.forEachIndexed{index, element ->
                        Log.d("ADDWORD", element.toString())
                        // TODO: Cambiar lenguaje de aprendizaje
                        viewModel.addWord(landmarks.get(0).location_lat, landmarks.get(0).location_long, element.objective,"English", "")
                    }
                }
                navController.navigate(Destinations.LANDMARKS_SCREEN)
            }
        }) {
        Icon(
            painter = painterResource(R.drawable.ic_round_check_24),
            contentDescription = null
        )
    }
}

@SuppressLint("MissingPermission")
@OptIn(
    ExperimentalMaterialApi::class, ExperimentalPermissionsApi::class,
    ExperimentalMaterialApi::class, ExperimentalMaterial3Api::class
)
@Composable
fun AddDetectedLandmarkComponent(
    navController: NavController,
    detectedLandmark : List<LandmarksDao>,
    viewModelLandmark:LandmarkDetectionViewModel = hiltViewModel(),
    detectedObjects: List<PredictionsDao>,
    viewModelObjects: ObjectRecognitionViewModel,
    selectedWords:MutableState<Set<Int>>
){
    val context = LocalContext.current
    val cm = LocalContext.current.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    Column (Modifier
        .fillMaxSize()){
        Spacer(modifier = Modifier.padding(top = 10.dp))
        LazyColumn(){
            itemsIndexed(detectedLandmark){index, item->
                Text(modifier =  Modifier.padding(start = 15.dp, top=5.dp),
                    text = item.originalName, style =  MaterialTheme.typography.bodyLarge, fontWeight = FontWeight.Bold)
                Text(modifier = Modifier.padding(top=25.dp,start = 15.dp, bottom = 10.dp),
                    text = item.englishName, style = MaterialTheme.typography.bodySmall
                )
                bitmapPhotoLandmark?.let {
                    Image(
                        modifier = Modifier
                            .fillMaxWidth(),
                        bitmap =  it.asImageBitmap(),
                        contentDescription = null,
                        contentScale = ContentScale.Fit
                    )
                }
            }
        }
        Spacer(modifier = Modifier.padding(top = 10.dp))
        Column(
            verticalArrangement = Arrangement.SpaceAround,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth()
        ) {
            Spacer(modifier = Modifier.padding(top = 10.dp))
            Text(text = "Select the words you want to add to your dictionary", textAlign = TextAlign.Center, fontSize = 16.sp)
        }
        LazyColumn(){
            itemsIndexed(detectedObjects){index, item->
                Card(
                    backgroundColor=MaterialTheme.colorScheme.primaryContainer.copy(0.5f),
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable {

                        }
                        .size(90.dp, 60.dp)
                        .padding(start = 10.dp, end = 10.dp, top = 5.dp, bottom = 5.dp)
                ){
                    Row(Modifier.fillMaxWidth()) {
                        Text(modifier =  Modifier.padding(start = 5.dp, top=5.dp),
                            text = item.objective, style =  MaterialTheme.typography.bodyLarge, fontWeight = FontWeight.Bold)
                        Text(modifier = Modifier.padding(top=25.dp,start = 15.dp),
                            text = item.native, style = MaterialTheme.typography.bodySmall
                        )
                        if (item.objective != "None of the above"){
                            val checked = selectedWords.value.contains(index)
                            Column(
                                modifier = Modifier.fillMaxWidth(),
                                horizontalAlignment = Alignment.End
                            ) {
                                Checkbox(
                                    checked = checked,
                                    onCheckedChange = {
                                        selectedWords.value = if (checked) {
                                            selectedWords.value - index
                                        } else {
                                            selectedWords.value + index
                                        }
                                    },
                                    colors = CheckboxDefaults.colors(md_theme_light_primary)
                                )
                            }
                        }
                    }
                }
            }
        }
    }
    if (detectedLandmark.isEmpty() && termino_Land){
        termino_Land=false
        Toast.makeText(LocalContext.current, "No landmark detected. Please try again.", Toast.LENGTH_LONG).show()
        navController.navigate(Destinations.LANDMARKS_SCREEN)
    }
    else{
        if (detectedObjects.size == 1){
            Toast.makeText(LocalContext.current, "New landmark added. No object detected", Toast.LENGTH_LONG).show()
        }
    }
}


@Composable
fun TopBarAddLandmarkAndWords(navController: NavController){
    SmallTopAppBar(
        navigationIcon = {
            IconButton(onClick = { navController.popBackStack() }) {
                IconButton(onClick = { navController.popBackStack() }) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Localized description"
                    )
                }
            }
        },
        title = {
            Text(
                text = "Add landmark",
                color=MaterialTheme.colorScheme.primary
            )
        },
        actions = {
             IconButton(onClick = {}){
                 Icon(
                     imageVector = Icons.Filled.Done,
                     contentDescription = "Done Arrow",
                     tint = MaterialTheme.colorScheme.primary
                 )
             }
        }
    )
}
