package com.moviles.langapp.views

import android.content.Context
import android.content.pm.ActivityInfo
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.text.isDigitsOnly
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.compose.LangappTheme
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.moviles.langapp.Destinations
import com.moviles.langapp.R
import com.moviles.langapp.utils.SignUpCache
import com.moviles.langapp.viewmodels.LoginScreenViewModel
import com.moviles.langapp.viewmodels.SignUpViewModel

val countriesList = mutableListOf<String>(
    "Afghanistan",
    "Åland Islands",
    "Albania",
    "Algeria",
    "American Samoa",
    "AndorrA",
    "Angola",
    "Anguilla",
    "Antarctica",
    "Antigua and Barbuda",
    "Argentina",
    "Armenia",
    "Aruba",
    "Australia",
    "Austria",
    "Azerbaijan",
    "Bahamas",
    "Bahrain",
    "Bangladesh",
    "Barbados",
    "Belarus",
    "Belgium",
    "Belize",
    "Benin",
    "Bermuda",
    "Bhutan",
    "Bolivia",
    "Bosnia and Herzegovina",
    "Botswana",
    "Bouvet Island",
    "Brazil",
    "British Indian Ocean",
    "Brunei Darussalam",
    "Bulgaria",
    "Burkina Faso",
    "Burundi",
    "Cambodia",
    "Cameroon",
    "Canada",
    "Cape Verde",
    "Cayman Islands",
    "Central African Republic",
    "Chad",
    "Chile",
    "China",
    "Christmas Island",
    "Cocos (Keeling) Islands",
    "Colombia",
    "Comoros",
    "Congo",
    "Congo, The Republic of the",
    "Cook Islands",
    "Costa Rica",
    "Cote D'Ivoire",
    "Croatia",
    "Cuba",
    "Cyprus",
    "Czech Republic",
    "Denmark",
    "Djibouti",
    "Dominica",
    "Dominican Republic",
    "Ecuador",
    "Egypt",
    "El Salvador",
    "Equatorial Guinea",
    "Eritrea",
    "Estonia",
    "Ethiopia",
    "Falkland Islands (Malvinas)",
    "Faroe Islands",
    "Fiji",
    "Finland",
    "France",
    "French Guiana",
    "French Polynesia",
    "French Southern Territories",
    "Gabon",
    "Gambia",
    "Georgia",
    "Germany",
    "Ghana",
    "Gibraltar",
    "Greece",
    "Greenland",
    "Grenada",
    "Guadeloupe",
    "Guam",
    "Guatemala",
    "Guernsey",
    "Guinea",
    "Guinea-Bissau",
    "Guyana",
    "Haiti",
    "Heard Island and Mcdonald Islands",
    "Holy See (Vatican City State)",
    "Honduras",
    "Hong Kong",
    "Hungary",
    "Iceland",
    "India",
    "Indonesia",
    "Iran, Islamic Republic Of",
    "Iraq",
    "Ireland",
    "Isle of Man",
    "Israel",
    "Italy",
    "Jamaica",
    "Japan",
    "Jersey",
    "Jordan",
    "Kazakhstan",
    "Kenya",
    "Kiribati",
    "Korea",
    "Korea, Republic of",
    "Kuwait",
    "Kyrgyzstan",
    "Lao People'S Democratic Republic",
    "Latvia",
    "Lebanon",
    "Lesotho",
    "Liberia",
    "Libyan Arab Jamahiriya",
    "Liechtenstein",
    "Lithuania",
    "Luxembourg",
    "Macao",
    "Macedonia",
    "Madagascar",
    "Malawi",
    "Malaysia",
    "Maldives",
    "Mali",
    "Malta",
    "Marshall Islands",
    "Martinique",
    "Mauritania",
    "Mauritius",
    "Mayotte",
    "Mexico",
    "Micronesia",
    "Moldova, Republic of",
    "Monaco",
    "Mongolia",
    "Montserrat",
    "Morocco",
    "Mozambique",
    "Myanmar",
    "Namibia",
    "Nauru",
    "Nepal",
    "Netherlands",
    "Netherlands Antilles",
    "New Caledonia",
    "New Zealand",
    "Nicaragua",
    "Niger",
    "Nigeria",
    "Niue",
    "Norfolk Island",
    "Northern Mariana Islands",
    "Norway",
    "Oman",
    "Pakistan",
    "Palau",
    "Palestinian Territory, Occupied",
    "Panama",
    "Papua New Guinea",
    "Paraguay",
    "Peru",
    "Philippines",
    "Pitcairn",
    "Poland",
    "Portugal",
    "Puerto Rico",
    "Qatar",
    "Reunion",
    "Romania",
    "Russian Federation",
    "RWANDA",
    "Saint Helena",
    "Saint Kitts and Nevis",
    "Saint Lucia",
    "Saint Pierre and Miquelon",
    "Saint Vincent and the Grenadines",
    "Samoa",
    "San Marino",
    "Sao Tome and Principe",
    "Saudi Arabia",
    "Senegal",
    "Serbia and Montenegro",
    "Seychelles",
    "Sierra Leone",
    "Singapore",
    "Slovakia",
    "Slovenia",
    "Solomon Islands",
    "Somalia",
    "South Africa",
    "Spain",
    "Sri Lanka",
    "Sudan",
    "Suriname",
    "Svalbard and Jan Mayen",
    "Swaziland",
    "Sweden",
    "Switzerland",
    "Syrian Arab Republic",
    "Taiwan, Province of China",
    "Tajikistan",
    "Tanzania",
    "Thailand",
    "Timor-Leste",
    "Togo",
    "Tokelau",
    "Tonga",
    "Trinidad and Tobago",
    "Tunisia",
    "Turkey",
    "Turkmenistan",
    "Turks and Caicos Islands",
    "Tuvalu",
    "Uganda",
    "Ukraine",
    "United Arab Emirates",
    "United Kingdom",
    "United States",
    "Uruguay",
    "Uzbekistan",
    "Vanuatu",
    "Venezuela",
    "Viet Nam",
    "Virgin Islands, British",
    "Virgin Islands, U.S.",
    "Wallis and Futuna",
    "Western Sahara",
    "Yemen",
    "Zambia",
    "Zimbabwe"
)


@Composable
fun SignUp1Screen(
    navController: NavController,
    viewModel: SignUpViewModel = hiltViewModel(),
    viewModelLogin: LoginScreenViewModel = hiltViewModel(),
) {
    LockScreenOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
    SignUp1Screen(
        navController = navController,
        viewModel,
        viewModel::createUser,
        viewModelLogin::login,
        viewModelLogin
    )
}


@Composable
fun CountrySelection(): String {
    var countrySelected: String by rememberSaveable {
        mutableStateOf(
            if (SignUpCache.instance.get_from_cache("Country_SignUp") != "") SignUpCache.instance.get_from_cache(
                "Country_SignUp"
            ) else countriesList[0]
        )
    }
    var expanded: Boolean by remember {
        mutableStateOf(false)
    }
    Box(Modifier.fillMaxWidth()) {
        Row(modifier = Modifier
            .padding(5.dp)
            .clickable {
                expanded = !expanded
            }
            .align(Alignment.Center),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(text = countrySelected, fontSize = 18.sp, modifier = Modifier.padding(end = 8.dp))
            Icon(imageVector = Icons.Filled.ArrowDropDown, contentDescription = "")

            DropdownMenu(expanded = expanded, onDismissRequest = {
                expanded = false
            }) {
                countriesList.forEach { country ->
                    DropdownMenuItem(text = { Text(text = country) }, onClick = {
                        expanded = false
                        countrySelected = country
                        SignUpCache.instance.save_in_cache("Country_SignUp", country)
                    })
                }
            }
        }
    }
    return countrySelected
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SignUp1Screen(
    navController: NavController,
    viewModel: SignUpViewModel,
    createUser: (String, String, String, String, String, String, String) -> Unit,
    login: (String, String) -> Unit,
    viewModelLogin: LoginScreenViewModel,
) {
    CheckSignedUp(navController = navController, viewModel = viewModel)
    NotificationMessageSignUp(viewModel)
    //NotificationMessage(viewModelLogin)
    val cm =
        LocalContext.current.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val lc = LocalContext.current

    var duolingo = "0"
    val systemUiController = rememberSystemUiController()
    systemUiController.setSystemBarsColor(
        MaterialTheme.colorScheme.background
    )

    Scaffold {
        Column(
            modifier = Modifier
                .padding(top = 45.dp, bottom = 30.dp)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Text("Welcome to", style = MaterialTheme.typography.headlineSmall)
            Text("LangApp", style = MaterialTheme.typography.displayLarge)
            Spacer(modifier = Modifier.padding(8.dp))
            Text("Enter your infromation", style = MaterialTheme.typography.headlineSmall)
            Spacer(modifier = Modifier.padding(8.dp))
            val focusManager = LocalFocusManager.current
            var text_name by rememberSaveable(stateSaver = TextFieldValue.Saver) {
                mutableStateOf(
                    TextFieldValue(
                        SignUpCache.instance.get_from_cache("Name_SignUp"),
                        TextRange(0, 7)
                    )
                )
            }
            OutlinedTextField(
                value = text_name,
                onValueChange = {
                    text_name = it
                },
                label = { Text("Name") },
                singleLine = true,
                keyboardActions = KeyboardActions(onDone = {
                    focusManager.clearFocus()
                    SignUpCache.instance.save_in_cache("Name_SignUp", text_name.text)
                })
            )
            Spacer(modifier = Modifier.padding(8.dp))
            var text_lastname by rememberSaveable(stateSaver = TextFieldValue.Saver) {
                mutableStateOf(
                    TextFieldValue(
                        SignUpCache.instance.get_from_cache("Lastname_SignUp"),
                        TextRange(0, 7)
                    )
                )
            }
            OutlinedTextField(
                value = text_lastname,
                onValueChange = { text_lastname = it },
                label = { Text("Lastname") },
                singleLine = true,
                keyboardActions = KeyboardActions(onDone = {
                    focusManager.clearFocus()
                    SignUpCache.instance.save_in_cache("Lastname_SignUp", text_lastname.text)
                })
            )
            Spacer(modifier = Modifier.padding(8.dp))
            Text(
                text = "Enter your country of residence",
                style = MaterialTheme.typography.bodyMedium
            )
            var country = CountrySelection()
            Spacer(modifier = Modifier.padding(3.dp))
            var birthday by rememberSaveable(stateSaver = TextFieldValue.Saver) {
                mutableStateOf(
                    TextFieldValue(
                        SignUpCache.instance.get_from_cache("Birthday_SignUp"),
                        TextRange(0, 7)
                    )
                )
            }
            OutlinedTextField(
                value = birthday,
                onValueChange = { birthday = it },
                label = { Text("Date of birth (DD-MM-YYYY)") },
                singleLine = true,
                keyboardActions = KeyboardActions(onDone = {
                    focusManager.clearFocus()
                    SignUpCache.instance.save_in_cache("Birthday_SignUp", birthday.text)
                })
            )
            Spacer(modifier = Modifier.padding(8.dp))
            var email by rememberSaveable(stateSaver = TextFieldValue.Saver) {
                mutableStateOf(TextFieldValue(SignUpCache.instance.get_from_cache("Email_SignUp")))
            }
            OutlinedTextField(
                value = email,
                onValueChange = { email = it },
                label = { Text("Email") },
                singleLine = true,
                keyboardActions = KeyboardActions(onDone = {
                    focusManager.clearFocus()
                    SignUpCache.instance.save_in_cache("Email_SignUp", email.text)
                })
            )
            Text(
                text = "@gmail.com,@outlook.com ...",
                style = MaterialTheme.typography.bodySmall,
                textAlign = TextAlign.Left
            )
            Spacer(modifier = Modifier.padding(8.dp))
            var password by rememberSaveable(stateSaver = TextFieldValue.Saver) {
                mutableStateOf(TextFieldValue(SignUpCache.instance.get_from_cache("Password_SignUp")))
            }
            OutlinedTextField(
                value = password,
                onValueChange = { password = it },
                label = { Text("Password") },
                visualTransformation = PasswordVisualTransformation(),
                singleLine = true,
                keyboardActions = KeyboardActions(onDone = {
                    focusManager.clearFocus()
                    SignUpCache.instance.save_in_cache("Password_SignUp", password.text)
                })
            )
            Spacer(modifier = Modifier.padding(8.dp))
            duolingo = RadioButtonDemo()
            Spacer(modifier = Modifier.weight(1f))
            Row {
                OutlinedButton(
                    onClick = { navController.navigate(Destinations.LOGIN_SCREEN) },
                    modifier = Modifier.size(64.dp, 40.dp)
                ) {
                    Icon(
                        painter = painterResource(R.drawable.ic_round_arrow_back_24),
                        contentDescription = null
                    )
                }
                Spacer(modifier = Modifier.padding(start = 30.dp))
                FilledTonalButton(
                    onClick = {
                        @Suppress("DEPRECATION") val isConnected: Boolean =
                            cm.activeNetworkInfo?.isConnectedOrConnecting == true
                        if (!isConnected) {
                            Toast.makeText(
                                lc,
                                "No connection. Please try again latter. ",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            if (email.text == "" || password.text == "") {
                                viewModel._dialogMessage.value = "Email or password empty."
                                viewModel._dialogMessage.value = ""
                            } else if (birthday.text == "" || text_lastname.text == "" || text_name.text == "" || country == "") {
                                viewModel._dialogMessage.value = "Please complete all the fields."
                                viewModel._dialogMessage.value = ""
                            } else if (checkValidity(birthday.text)) {
                                viewModel._dialogMessage.value =
                                    "There is an error in the date format"
                                viewModel._dialogMessage.value = ""
                            } else {
                                navController.navigate(Destinations.ONBOARDNAT_SCREEN)
                                createUser(
                                    email.text,
                                    password.text,
                                    birthday.text,
                                    text_lastname.text,
                                    text_name.text,
                                    country,
                                    duolingo
                                )
                                login(email.text, password.text)
                            }

                        }
                    },
                    modifier = Modifier.size(175.dp, 35.dp)
                ) {
                    Text(text = "Sign Up")
                }
            }


        }
    }
}

fun checkValidity(birthday: String): Boolean {
    val splittedBirthday = birthday.split("-")
    if (splittedBirthday.size != 3) {
        return false
    } else if (!splittedBirthday[0].isDigitsOnly() || 1 < splittedBirthday[0].toInt() || splittedBirthday[0].toInt() > 31) {
        return false
    } else if (!splittedBirthday[1].isDigitsOnly() || 1 < splittedBirthday[1].toInt() || splittedBirthday[1].toInt() > 12) {
        return false
    } else if (!splittedBirthday[2].isDigitsOnly() || 1900 < splittedBirthday[1].toInt() || splittedBirthday[1].toInt() > 2021) {
        return false
    }
    return true
}

var duolingo= ""

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RadioButtonDemo(): String {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val selected = remember { mutableStateOf("0") }
        Row {
            Column {
                RadioButton(selected = selected.value == "1",
                    onClick = {
                        if (selected.value == "1") {
                            selected.value = "0"
                        } else {
                            selected.value = "1"
                        }
                        duolingo = selected.value
                    })
                Spacer(modifier = Modifier.size(2.dp))
            }
            Column {
                Spacer(modifier = Modifier.padding(top = 12.dp))
                Text("I am a former/current Duolingo user")
            }
        }
    }
    return duolingo
}


@Preview(showBackground = true)
@Composable
fun SignUp1ScreenPreview() {
    LangappTheme {
        SignUp1Screen(navController = rememberNavController())
    }
}