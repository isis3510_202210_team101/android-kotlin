package com.moviles.langapp.views

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.moviles.langapp.Destinations
import android.widget.Toast
import androidx.compose.foundation.selection.selectable
import androidx.compose.ui.platform.LocalContext
import com.moviles.langapp.model.Languages
import com.moviles.langapp.utils.LangappContext
import com.moviles.langapp.utils.OnboardingCache
import com.moviles.langapp.utils.SharedPref
import com.moviles.langapp.viewmodels.SelectLearningLangVM

@Composable
fun SelectLearningLangScreen(
    navController: NavController,
    viewModel: SelectLearningLangVM = hiltViewModel()
) {
    val learningList by viewModel.getLearningLangs().observeAsState(initial = emptyList())
    SelectLearningLangScreen(navController, learningList, viewModel)
}

@OptIn(ExperimentalMaterial3Api::class, coil.annotation.ExperimentalCoilApi::class)
@Composable
fun SelectLearningLangScreen(
    navController: NavController,
    lanList: List<Languages>,
    viewModel: SelectLearningLangVM
) {
    fun checkForInternet(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            @Suppress("DEPRECATION") val networkInfo =
                connectivityManager.activeNetworkInfo ?: return false
            @Suppress("DEPRECATION")
            return networkInfo.isConnected
        }
    }

    val context = LocalContext.current

    var selected:Languages = Languages("","")


    fun mapToDefaultLanguage(language: Languages): String{
        return when (language.language){
            "English" -> {
                "English"
            }
            "Deutsch" -> {
                "German"
            }
            "Italiano" -> {
                "Italian"
            }
            "Português" -> {
                "Portuguese"
            }
            else ->{
                "Spanish"
            }
        }
    }

    @Composable
    fun FAB(navController: NavController) {
        val context = LangappContext.getContext()
        val prefs = SharedPref.getInstance(context)
        FloatingActionButton(
            onClick = {
                if(checkForInternet(context)){
                    if (selected.language != ""){
                        prefs.save("LEARNINGLANGUAGE",mapToDefaultLanguage(selected))
                        OnboardingCache.instance.save_in_cache("LEARNINGLANGUAGE",mapToDefaultLanguage(selected))
                        navController.popBackStack()

                    }
                }else{
                    Toast.makeText(context,"You don't have internet connection",Toast.LENGTH_SHORT).show()
                }
            }) {
            Icon(
                imageVector = Icons.Filled.Check,
                contentDescription = "Define learning lang",
                tint = MaterialTheme.colorScheme.primary
            )
        }
    }
    @Composable
    fun SimpleRadioButtonComponent() {
        val radioOptions = listOf("")
        val (selectedOption, onOptionSelected) = remember { mutableStateOf(radioOptions[0]) }
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .padding(top = 15.dp),

            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Column {
                lanList.forEach { text ->
                    Card(
                        backgroundColor = Color.LightGray,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(10.dp)
                            .selectable(
                                selected = (text.language == selectedOption),
                                onClick = {
                                    onOptionSelected(text.language)
                                    selected.flag = text.flag
                                }
                            ),
                        elevation = 10.dp,
                        shape = RoundedCornerShape(10.dp)
                    ){
                        val context = LocalContext.current

                        RadioButton(
                            modifier = Modifier
                                .padding(bottom = 5.dp,start = 300.dp),
                            selected = (text.language == selectedOption),
                            onClick = {
                                onOptionSelected(text.language)
                                selected.flag = text.flag
                            }
                        )
                        Text(
                            text = text.language,
                            style = MaterialTheme.typography.titleLarge,
                            modifier = Modifier.padding(top = 15.dp,start = 22.dp)
                        )
                    }

                }
            }
        }
        selected.language = selectedOption
    }



    @Composable
    fun TopBarHomeDict() {

        SmallTopAppBar(
            title = {
                Text(
                    "Select your language",
                    color = MaterialTheme.colorScheme.primary,
                    style = MaterialTheme.typography.headlineSmall
                )
            },
            actions = {
                IconButton(onClick = { /* doSomething() */ }) {
                    Icon(
                        imageVector = Icons.Filled.Search,
                        contentDescription = "Localize language",
                        tint = MaterialTheme.colorScheme.primary
                    )
                }
            }
        )
    }


    Scaffold(
        topBar = { TopBarHomeDict() },
        floatingActionButton = { FAB(navController) }
    ) {
        SimpleRadioButtonComponent()
    }
}


