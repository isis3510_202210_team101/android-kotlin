package com.moviles.langapp.views

import android.content.pm.ActivityInfo
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.OutlinedTextField
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.compose.LangappTheme
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.moviles.langapp.Destinations
import com.moviles.langapp.R
import com.moviles.langapp.viewmodels.LoginScreenViewModel


@Composable
fun LoginCredentialsScreen(
    navController: NavController,
    viewModel: LoginScreenViewModel = hiltViewModel()
) {
    LoginCredentialsScreen(navController = navController, viewModel, viewModel::login)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LoginCredentialsScreen(
    navController: NavController,
    viewModel: LoginScreenViewModel,
    onLogin: (String, String) -> Unit
) {
    LockScreenOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
    CheckSignedIn(navController, viewModel)
    NotificationMessage(viewModel)


    var email by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(TextFieldValue(""))
    }
    var password by rememberSaveable(stateSaver = TextFieldValue.Saver) {
        mutableStateOf(TextFieldValue(""))
    }

    val systemUiController = rememberSystemUiController()
    systemUiController.setSystemBarsColor(
        MaterialTheme.colorScheme.background
    )


    Scaffold {

        Column(
            modifier = Modifier
                .padding(top = 45.dp, bottom = 90.dp)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Text("Welcome to", style = MaterialTheme.typography.headlineSmall)
            Text("LangApp", style = MaterialTheme.typography.displayLarge)
            Spacer(modifier = Modifier.padding(20.dp))
            Text("It's nice to have you back", style = MaterialTheme.typography.headlineSmall)
            Spacer(modifier = Modifier.padding(60.dp))


            val focusManager = LocalFocusManager.current

            OutlinedTextField(
                value = email,
                onValueChange = { email = it },
                placeholder = { Text("example@gmail.com") },
                label = { Text("Email") },
                singleLine = true,
                keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() })
            )
            Spacer(modifier = Modifier.padding(30.dp))
            OutlinedTextField(
                value = password,
                onValueChange = { password = it },
                label = { Text("Password") },
                visualTransformation = PasswordVisualTransformation(),
                singleLine = true,
                keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() })
            )
            Spacer(modifier = Modifier.weight(1f))
            Row {

                OutlinedButton(
                    onClick = { navController.navigate(Destinations.LOGIN_SCREEN) },
                    modifier = Modifier.size(64.dp, 40.dp)
                ) {
                    Icon(
                        painter = painterResource(R.drawable.ic_round_arrow_back_24),
                        contentDescription = null
                    )
                }
                Spacer(modifier = Modifier.padding(start = 30.dp))
                FilledTonalButton(
                    onClick = {
                        onLogin(email.text, password.text)
                    },
                    modifier = Modifier.size(175.dp, 40.dp)
                ) {
                    Text(text = "Log In")
                }
            }
        }

    }
}

@Preview(showBackground = true)
@Composable
fun LoginCredentialsPreview() {
    LangappTheme {
        LoginCredentialsScreen(
            navController = rememberNavController(),
            viewModel = LoginScreenViewModel(hiltViewModel())
        )
    }
}