package com.moviles.langapp.views

import android.content.pm.ActivityInfo
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.compose.LangappTheme
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.moviles.langapp.Destinations
import com.moviles.langapp.R


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LoginScreen(
    navController: NavController
) {
    LockScreenOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)

    val systemUiController = rememberSystemUiController()
    systemUiController.setSystemBarsColor(
        MaterialTheme.colorScheme.background
    )

    Scaffold {

        Column(
            modifier = Modifier
                .padding(top = 45.dp, bottom = 90.dp)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Text("Welcome to", style = MaterialTheme.typography.headlineSmall)
            Text("LangApp", style = MaterialTheme.typography.displayLarge)
            Spacer(modifier = Modifier.padding(20.dp))
            Text("Let's get started", style = MaterialTheme.typography.headlineSmall)
            Spacer(modifier = Modifier.padding(10.dp))
            Image(
                modifier = Modifier
                    .fillMaxWidth(),
                painter = painterResource(R.drawable.sitting),
                contentDescription = null,
                contentScale = ContentScale.FillWidth
            )
            Spacer(modifier = Modifier.weight(1f))
            Column {
                FilledTonalButton(
                    onClick = { navController.navigate(Destinations.LOGIN_CREDENTIALS) },
                    modifier = Modifier.size(175.dp, 35.dp)
                ) {
                    Text(text = "Login")
                }
                Spacer(modifier = Modifier.padding(20.dp))
                OutlinedButton(
                    onClick = { navController.navigate(Destinations.SIGN_UP_1) },
                    modifier = Modifier.size(175.dp, 35.dp)
                ) {
                    Text(text = "Sign up")
                }
            }
        }

    }
}


@Preview(showBackground = true)
@Composable
fun LoginPreview() {
    LangappTheme {
        LoginScreen(navController = rememberNavController())
    }
}

