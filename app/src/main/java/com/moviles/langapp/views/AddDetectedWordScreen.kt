package com.moviles.langapp.views

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.OutlinedTextField
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.rememberPermissionState
import com.google.android.gms.location.FusedLocationProviderClient
import com.moviles.langapp.Destinations
import com.moviles.langapp.utils.AddWordCache
import com.moviles.langapp.viewmodels.AddWordScreenViewModel
import com.moviles.langapp.viewmodels.LoginScreenViewModel

//var predicted_labels:List<PredictionsDao>?=null

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddDetectedWordScreen(
    navController: NavController,
    fusedLocationProviderClient: FusedLocationProviderClient,
    viewModel: LoginScreenViewModel = hiltViewModel()
) {
    LockScreenOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
    Scaffold(
        topBar = { TopBarAddWord(navController) },
        bottomBar = { NavBarHome(navController) }

    ) {
        AddDetectedWordComponent(fusedLocationProviderClient, navController = navController)
    }
}

@SuppressLint("MissingPermission")
@OptIn(
    ExperimentalMaterialApi::class,
    com.google.accompanist.permissions.ExperimentalPermissionsApi::class,
    androidx.compose.material.ExperimentalMaterialApi::class
)
@Composable
fun AddDetectedWordComponent(
    fusedLocationProviderClient: FusedLocationProviderClient,
    navController: NavController,
    viewModel: AddWordScreenViewModel = hiltViewModel()
) {
    val context = LocalContext.current
    val cm =
        LocalContext.current.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    Column {
        bitmapPhoto?.let {
            Image(
                modifier = Modifier
                    .fillMaxWidth(),
                bitmap = it.asImageBitmap(),
                contentDescription = null,
                contentScale = ContentScale.Fit
            )
        }
        val locationPermissionState =
            rememberPermissionState(Manifest.permission.ACCESS_COARSE_LOCATION)
        Spacer(modifier = Modifier.padding(top = 10.dp))
        Column(
            verticalArrangement = Arrangement.SpaceAround,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth()
        ) {
            val focusManager = LocalFocusManager.current
            var langauge_text = AddWordCache.instance.get_from_cache("languageToAdd")
            OutlinedTextField(
                value = langauge_text,
                onValueChange = { langauge_text = it },
                label = { Text("Language") },
                singleLine = true,
                keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() })
            )
            var word by rememberSaveable(stateSaver = TextFieldValue.Saver) {
                mutableStateOf(
                    TextFieldValue(
                        if (AddWordCache.instance.get_from_cache("selected_Word") != "") AddWordCache.instance.get_from_cache(
                            "selected_Word"
                        ) else ""
                    )
                )
            }
            Spacer(modifier = Modifier.padding(top = 30.dp))
            OutlinedTextField(
                value = word,
                onValueChange = { word = it },
                placeholder = { Text("") },
                label = { Text("Word to add") },
                singleLine = true,
                keyboardActions = KeyboardActions(onDone = {
                    focusManager.clearFocus()
                    AddWordCache.instance.save_in_cache("selected_Word", word.text)
                })
            )
            Spacer(modifier = Modifier.padding(top = 10.dp))
            Button(onClick = {
                @Suppress("DEPRECATION") val isConnected: Boolean =
                    cm.activeNetworkInfo?.isConnectedOrConnecting == true
                if (!isConnected) {
                    Toast.makeText(
                        context,
                        "No connection. You need connection to add a word. Please try again latter.",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    if (locationPermissionState.status is PermissionStatus.Granted) {
                        fusedLocationProviderClient.lastLocation.addOnSuccessListener {
                            viewModel.addWord(
                                it.latitude,
                                it.longitude,
                                AddWordCache.instance.get_from_cache("selected_Word"),
                                langauge_text,
                                AddWordCache.instance.get_from_cache("path_post")
                            )
                            AddWordCache.instance.clear_cache()
                            bitmapPhoto = null
                            navController.navigate(Destinations.HOME_SCREEN)
                        }
                    }
                }
            }) {
                Text(text = "Add word")
            }
        }
    }
}