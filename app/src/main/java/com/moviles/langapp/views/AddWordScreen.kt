package com.moviles.langapp.views

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.launch
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ExposedDropdownMenuBox
import androidx.compose.material.ExposedDropdownMenuDefaults
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.rememberPermissionState
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.moviles.langapp.Destinations
import com.moviles.langapp.R
import com.moviles.langapp.utils.AddWordCache
import com.moviles.langapp.viewmodels.AddWordScreenViewModel
import com.moviles.langapp.viewmodels.LoginScreenViewModel
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

var bitmapPhoto: Bitmap? = null

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddWordScreen(
    navController: NavController,
    fusedLocationProviderClient: FusedLocationProviderClient,
    viewModel: LoginScreenViewModel = hiltViewModel()
) {

    LockScreenOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
    Scaffold(
        topBar = { TopBarAddWord(navController) },
        bottomBar = { NavBarHome(navController) }

    ) {
        AddWordComponent(fusedLocationProviderClient, navController)
    }
}

@SuppressLint("MissingPermission")
@OptIn(
    ExperimentalMaterialApi::class,
    com.google.accompanist.permissions.ExperimentalPermissionsApi::class,
    androidx.compose.material.ExperimentalMaterialApi::class
)
@Composable
fun AddWordComponent(
    fusedLocationProviderClient: FusedLocationProviderClient,
    navController: NavController,
    viewModel: AddWordScreenViewModel = hiltViewModel()
) {

    val cm =
        LocalContext.current.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val languages = listOf("English", "Spanish", "German", "Italian", "Portuguese")
    var selectedOptionText by remember {
        mutableStateOf(
            if (AddWordCache.instance.get_from_cache("languageToAdd") != "") AddWordCache.instance.get_from_cache(
                "languageToAdd"
            ) else languages[0]
        )
    }
    if (selectedOptionText == "") {
        languages[0]
    }
    AddWordCache.instance.save_in_cache("languageToAdd", selectedOptionText)
    val context = LocalContext.current
    val cameraLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.TakePicturePreview()
    ) {

            btm: Bitmap? ->
        bitmapPhoto = btm
        val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
        val now = Date()
        val filename = formatter.format(now)
        var storage = FirebaseStorage.getInstance()
        val storageRef = storage.reference
        val mAuth = FirebaseAuth.getInstance()
        mAuth.signInAnonymously()
        val ImageRef = storageRef.child("words/$filename.jpg")
        val baos = ByteArrayOutputStream()
        bitmapPhoto?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()
        var uploadTask = ImageRef.putBytes(data)
        uploadTask.addOnFailureListener {
            Toast.makeText(
                context,
                "Something went wrong when saving the photo. Please check your conectivity and try again.",
                Toast.LENGTH_LONG
            ).show()
        }.addOnSuccessListener { taskSnapshot ->
            run {
                ImageRef.downloadUrl.addOnSuccessListener { uri ->
                    run {
                        when (selectedOptionText) {
                            "English" -> {
                                AddWordCache.instance.save_in_cache("target_post", "en")
                            }
                            "German" -> {
                                AddWordCache.instance.save_in_cache("target_post", "de")
                            }
                            "Italian" -> {
                                AddWordCache.instance.save_in_cache("target_post", "it")
                            }
                            "Portuguese" -> {
                                AddWordCache.instance.save_in_cache("target_post", "pt")
                            }
                            "Spanish" -> {
                                AddWordCache.instance.save_in_cache("target_post", "es")
                            }
                        }
                        AddWordCache.instance.save_in_cache("path_post", uri.toString())
                        @Suppress("DEPRECATION") val isConnected: Boolean =
                            cm.activeNetworkInfo?.isConnectedOrConnecting == true
                        if (!isConnected) {
                            Toast.makeText(
                                context,
                                "No connection. You need connection to process the photo. Please try again latter.",
                                Toast.LENGTH_LONG
                            ).show()
                        } else {
                            navController.navigate(Destinations.SELECT_DETECTED_WORDS)
                            Toast.makeText(
                                context,
                                "Processing the image. Please wait",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }.addOnFailureListener {

                }
            }
        }

    }
    val permissionLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            @Suppress("DEPRECATION") val isConnected: Boolean =
                cm.activeNetworkInfo?.isConnectedOrConnecting == true
            if (!isConnected) {
                Toast.makeText(
                    context,
                    "No connection. You need connection to save the photo. Please try again latter.",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                cameraLauncher.launch()
            }
        } else {
            Toast.makeText(context, "Permission Denied!", Toast.LENGTH_SHORT).show()
        }
    }




    Column {
        Image(
            modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    when (PackageManager.PERMISSION_GRANTED) {
                        ContextCompat.checkSelfPermission(
                            context, android.Manifest.permission.CAMERA
                        ) -> {
                            @Suppress("DEPRECATION") val isConnected: Boolean =
                                cm.activeNetworkInfo?.isConnectedOrConnecting == true
                            if (!isConnected) {
                                Toast
                                    .makeText(
                                        context,
                                        "No connection. You need connection to save the photo. Please try again latter.",
                                        Toast.LENGTH_LONG
                                    )
                                    .show()
                            } else {
                                cameraLauncher.launch()
                            }
                        }
                        else -> {
                            permissionLauncher.launch(android.Manifest.permission.CAMERA)
                        }
                    }
                },
            painter = painterResource(R.drawable.no_image),
            contentDescription = null,
            contentScale = ContentScale.FillWidth
        )
        val locationPermissionState =
            rememberPermissionState(Manifest.permission.ACCESS_COARSE_LOCATION)

        var word by rememberSaveable(stateSaver = TextFieldValue.Saver) {
            mutableStateOf(
                TextFieldValue(
                    if (AddWordCache.instance.get_from_cache("Word2Add") != "") AddWordCache.instance.get_from_cache(
                        "Word2Add"
                    ) else ""
                )
            )
        }
        var isError by remember { mutableStateOf(false) }

        var expanded by remember { mutableStateOf(false) }
        var enabled by remember { mutableStateOf(false) }
        enabled = word.text != ""

        Spacer(modifier = Modifier.padding(top = 10.dp))
        Column(
            verticalArrangement = Arrangement.SpaceAround,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth()
        ) {
            val focusManager = LocalFocusManager.current



            ExposedDropdownMenuBox(
                expanded = expanded,
                onExpandedChange = {
                    expanded = !expanded
                }
            ) {
                OutlinedTextField(
                    readOnly = true,
                    value = selectedOptionText,
                    onValueChange = { },
                    label = { Text("Language") },
                    trailingIcon = {
                        ExposedDropdownMenuDefaults.TrailingIcon(
                            expanded = expanded
                        )
                    }
                )

                ExposedDropdownMenu(
                    expanded = expanded,
                    onDismissRequest = {
                        expanded = false
                    }
                ) {
                    languages.forEach { selectionOption ->
                        DropdownMenuItem(
                            onClick = {
                                AddWordCache.instance.save_in_cache(
                                    "languageToAdd",
                                    selectionOption
                                )
                                selectedOptionText = selectionOption
                                expanded = false
                            },
                            text = { Text(selectionOption) }
                        )
                    }
                }
            }
            Spacer(modifier = Modifier.padding(top = 30.dp))
            Column(
                modifier = Modifier
                    .padding(8.dp)
            ) {
                OutlinedTextField(
                    value = word,
                    onValueChange = {
                        word = it
                        isError = it.text == ""
                    },
                    placeholder = { Text("Example") },
                    label = { Text("Word to add") },
                    singleLine = true,
                    keyboardActions = KeyboardActions(onDone = {
                        focusManager.clearFocus()
                        AddWordCache.instance.save_in_cache("Word2Add", word.text)
                    }),
                    isError = isError
                )
                if (isError) {
                    Text(
                        text = "Error message",
                        color = MaterialTheme.colorScheme.error,
                        style = MaterialTheme.typography.bodySmall,
                        modifier = Modifier.padding(start = 16.dp)
                    )
                }
            }
            Spacer(modifier = Modifier.padding(top = 10.dp))
            Button(
                enabled = enabled,
                onClick = {
                    @Suppress("DEPRECATION") val isConnected: Boolean =
                        cm.activeNetworkInfo?.isConnectedOrConnecting == true
                    if (!isConnected) {
                        Toast.makeText(
                            context,
                            "No connection. You need connection to add a word. Please try again latter.",
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        if (locationPermissionState.status is PermissionStatus.Granted) {
                            fusedLocationProviderClient.lastLocation.addOnSuccessListener {
                                if(it != null){
                                    viewModel.addWord(
                                        it.latitude,
                                        it.longitude,
                                        word.text,
                                        selectedOptionText,
                                        ""
                                    )
                                    //navController.popBackStack()
                                    AddWordCache.instance.clear_cache()
                                    navController.navigate(Destinations.HOME_SCREEN)
                                }else{
                                    Toast.makeText(context, "There was an error please retry later", Toast.LENGTH_LONG).show()

                                }
                            }
                        }else{
                            Toast.makeText(context, "Please grant access to location", Toast.LENGTH_LONG).show()
                        }
                    }
                }) {
                Text("Add word")
            }

        }

    }
}

@Composable
fun TopBarAddWord(navController: NavController) {

    SmallTopAppBar(
        navigationIcon = {
            IconButton(onClick = { navController.popBackStack() }) {
                Icon(
                    imageVector = Icons.Filled.ArrowBack,
                    contentDescription = "Localized description"
                )
            }
        },
        title = {
            Text(
                "Add new word",
                color = MaterialTheme.colorScheme.primary
            )
        },
        actions = {
            IconButton(onClick = { /* doSomething() */ }) {
                Icon(
                    imageVector = Icons.Filled.Done,
                    contentDescription = "Done Arrow",
                    tint = MaterialTheme.colorScheme.primary
                )
            }
        }
    )
}
