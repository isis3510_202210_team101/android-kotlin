package com.moviles.langapp.views

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.widget.Toast
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavController
import com.moviles.langapp.Destinations
import com.moviles.langapp.viewmodels.DictionaryViewModel
import com.moviles.langapp.viewmodels.LoginScreenViewModel
import com.moviles.langapp.viewmodels.SignUpViewModel


@Composable
fun CheckSignedUp(navController: NavController, viewModel: SignUpViewModel) {
    val success: Boolean by viewModel.success.observeAsState(false)
    val alreadyLoggedIn = remember { mutableStateOf(false) }
    if (success && !alreadyLoggedIn.value) {
        alreadyLoggedIn.value = true
        navController.navigate(Destinations.ONBOARDNAT_SCREEN) {
            popUpTo(0)
        }
    }
}

@Composable
fun CheckSignedIn(navController: NavController, viewModel: LoginScreenViewModel) {
    val alreadyLoggedIn = remember { mutableStateOf(false) }
    val success: Boolean by viewModel.success.observeAsState(false)

    if (success && !alreadyLoggedIn.value) {
        alreadyLoggedIn.value = true
        navController.navigate(Destinations.HOME_SCREEN) {
            popUpTo(0)
        }
    }
}

@Composable
fun NotificationMessageSignUp(viewModel: SignUpViewModel) {
    val dialogMessage: String by viewModel.dialogMessage.observeAsState("")
    if (dialogMessage != "") {
        Toast.makeText(LocalContext.current, dialogMessage, Toast.LENGTH_SHORT).show()
    }
}

@Composable
fun NotificationMessage(viewModel: LoginScreenViewModel) {
    val dialogMessage: String by viewModel.dialogMessage.observeAsState("")
    if (dialogMessage != "") {
        Toast.makeText(LocalContext.current, dialogMessage, Toast.LENGTH_SHORT).show()
    }
}

@Composable
fun NotificationMessageDictionary(viewModel: DictionaryViewModel) {
    val dialogMessage: String by viewModel.dialogMessage.observeAsState("")
    if (dialogMessage != "") {
        Toast.makeText(LocalContext.current, dialogMessage, Toast.LENGTH_SHORT).show()
    }
}

@Composable
fun LockScreenOrientation(orientation: Int) {
    val context = LocalContext.current
    DisposableEffect(Unit) {
        val activity = context.findActivity() ?: return@DisposableEffect onDispose {}
        val originalOrientation = activity.requestedOrientation
        activity.requestedOrientation = orientation
        onDispose {
            // restore original orientation when view disappears
            activity.requestedOrientation = originalOrientation
        }
    }
}

fun Context.findActivity(): Activity? = when (this) {
    is Activity -> this
    is ContextWrapper -> baseContext.findActivity()
    else -> null
}