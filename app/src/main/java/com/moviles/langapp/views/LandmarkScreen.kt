package com.moviles.langapp.views
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.launch
import androidx.compose.animation.*
import androidx.compose.animation.core.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import com.example.compose.md_theme_light_onSurface
import com.example.compose.md_theme_light_primary
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.moviles.langapp.Destinations
import com.moviles.langapp.R
import com.moviles.langapp.model.DictionaryWord
import com.moviles.langapp.model.Landmark
import com.moviles.langapp.utils.AddLandmarkCache
import com.moviles.langapp.utils.Constants.CollapseAnimation
import com.moviles.langapp.utils.Constants.ExpandAnimation
import com.moviles.langapp.utils.Constants.FadeInAnimation
import com.moviles.langapp.utils.Constants.FadeOutAnimation
import com.moviles.langapp.viewmodels.LandmarkDetectionViewModel
import com.moviles.langapp.viewmodels.LandmarkViewModel
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

var bitmapPhotoLandmark: Bitmap?=null

@Composable
fun LandmarkScreen(
    navController: NavController,
    viewModelLandmarkDetection: LandmarkDetectionViewModel = hiltViewModel(),
    viewModelLandmark: LandmarkViewModel = hiltViewModel()){

    viewModelLandmark.getUserLandrmarks()
    LandmarkScreen2(navController = navController, viewModelLandmarkDetection, viewModelLandmark)

}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalAnimationApi::class)
@Composable
fun LandmarkScreen2(
    navController: NavController,
    viewModelLandmarkDetection: LandmarkDetectionViewModel,
    viewModelLandmark: LandmarkViewModel
) {

    val landmarks by viewModelLandmark.landmarks.collectAsState()

    Scaffold(
        topBar = { TopBarHomeLand(navController) },
        bottomBar = { NavBarHome(navController) },
        floatingActionButton = {FABLand(navController, viewModel=viewModelLandmarkDetection)}
    ) {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(25.dp),
            contentPadding = it
        ) {
            items(landmarks){ landmark ->
                LandmarksCard(landmark)

            }
        }
    }
}

@OptIn(ExperimentalCoilApi::class)
@Composable
fun LandmarksCard( landmark: Landmark){


    Card(
        backgroundColor = Color(0xFFE4DFFF),
        shape = RoundedCornerShape(15.dp),
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = 20.dp,
                vertical = 8.dp
            )
    ){
        Column(
            modifier = Modifier.fillMaxWidth()
        ){
            Column(modifier = Modifier.fillMaxWidth())
            {
                Column(
                    modifier = Modifier.padding(10.dp)
                ) {
                    Text(
                        text = landmark.originalName,
                        color = md_theme_light_onSurface,
                        fontWeight = FontWeight.Bold,
                        style = MaterialTheme.typography.titleMedium,
                        modifier = Modifier.fillMaxWidth()
                    )
                    Spacer(modifier = Modifier.padding(vertical = 7.dp))
                    Text(
                        text = landmark.translatedName, color = md_theme_light_onSurface,
                        modifier = Modifier.fillMaxWidth()
                    )
                }
                Image(
                    modifier = Modifier.fillMaxWidth().height(200.dp),
                    painter = rememberImagePainter(
                        data = landmark.photo,
                        builder = {
                            placeholder(R.drawable.placeholder)
                            error(R.drawable.placeholder)
                        }
                    ),
                    contentDescription = "Imagen",
                    contentScale = ContentScale.FillWidth
                )
            }
        }
    }
}

@Composable
fun FABLand(navController: NavController, viewModel:LandmarkDetectionViewModel){
    val cm = LocalContext.current.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val cl = LocalContext.current
    val cameraLauncher =  rememberLauncherForActivityResult(
        contract = ActivityResultContracts.TakePicturePreview()){
            btm: Bitmap? ->
        bitmapPhotoLandmark = btm
        Toast.makeText(cl,"Processing the image to detect landmarks. Please wait", Toast.LENGTH_LONG).show()
        val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
        val now = Date()
        val filename =  formatter.format(now)
        val storage = FirebaseStorage.getInstance()
        val storageRef = storage.reference
        val mAuth = FirebaseAuth.getInstance()
        mAuth.signInAnonymously()
        val ImageRef = storageRef.child("landmarks/$filename.jpg")
        val baos = ByteArrayOutputStream()
        bitmapPhotoLandmark?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()
        var uploadTask = ImageRef.putBytes(data)
        uploadTask.addOnFailureListener{
            Toast.makeText(cl,"Something went wrong when saving the photo. Please check your conectivity and try again.", Toast.LENGTH_LONG).show()
        }.addOnSuccessListener { taskSanpshot ->
            run{
                ImageRef.downloadUrl.addOnSuccessListener { uri ->
                    run{
                        AddLandmarkCache.instance.save_in_cache("path_post", uri.toString())
                        @Suppress("DEPRECATION") val isConnected:Boolean = cm.activeNetworkInfo?.isConnectedOrConnecting== true
                        if (!isConnected){
                            Toast.makeText(cl,"No connection. You need connection to process the photo. Please try again latter.", Toast.LENGTH_LONG).show()
                        }else{
                            navController.navigate(Destinations.ADD_LANDMARK_AND_WORDS)
                        }
                    }
                }.addOnFailureListener{

                }
            }
        }

    }
    val permissionLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission()){
            isGranted:Boolean ->
        if (isGranted){
            @Suppress("DEPRECATION") val isConnected:Boolean = cm.activeNetworkInfo?.isConnectedOrConnecting== true
            if (!isConnected){

            }else{
                cameraLauncher.launch()
            }
        }else{
        Toast.makeText(cl, "Permission Denied!", Toast.LENGTH_SHORT).show()
    }
    }

    FloatingActionButton(
        onClick = {
            @Suppress("DEPRECATION") val isConnected:Boolean = cm.activeNetworkInfo?.isConnectedOrConnecting== true
            if (!isConnected){
                Toast.makeText(cl, "No connection. You need connection to add a landmark. Please try again latter.", Toast.LENGTH_LONG).show()
            }else{
                when (PackageManager.PERMISSION_GRANTED){
                    ContextCompat.checkSelfPermission(
                        cl, android.Manifest.permission.CAMERA
                    )->{
                        @Suppress("DEPRECATION") val isConnected:Boolean = cm.activeNetworkInfo?.isConnectedOrConnecting==true
                        if (!isConnected){
                            Toast.makeText(cl, "No connection. You need connection to add a landmark. Please try again latter.", Toast.LENGTH_LONG).show()
                        }else{
                            cameraLauncher.launch()
                        }
                     }
                    else ->{
                        permissionLauncher.launch(android.Manifest.permission.CAMERA)
                    }
                }
            }
        }) {
        Icon(
            painter = painterResource(R.drawable.ic_round_camera_alt_24),
            contentDescription = null
        )
    }
}

@Composable
fun TopBarHomeLand(navController: NavController) {

    SmallTopAppBar(
        title = {
            Text(
                "My Landmarks",
                color = MaterialTheme.colorScheme.primary,
                style = MaterialTheme.typography.headlineSmall)
        },
        actions = {
            IconButton(onClick = {
                navController.navigate(Destinations.SELECT_LEARN_LANG)
            }) {
                Image(
                    painter = painterResource(R.drawable.spain_flag),
                    contentDescription = "Localized description"
                )
            }
            IconButton(onClick = { /* doSomething() */ }) {
                Icon(
                    imageVector = Icons.Filled.Search,
                    contentDescription = "Localized description",
                    tint = MaterialTheme.colorScheme.primary
                )
            }
        }
    )
}

@OptIn(ExperimentalMaterialApi::class)
@ExperimentalAnimationApi
@SuppressLint("UnusedTransitionTargetStateParameter")
@Composable
//falta agregar el translation en el not expanded card
fun ExpandableCardLandmark(
    card: DictionaryWord?,
    onClick: () -> Unit,
    expanded: Boolean
) {
    if(card == null){
        return
    }
    val interactionSource = remember { MutableInteractionSource() }
    val transitionState = remember { MutableTransitionState(expanded).apply {
        targetState = !expanded
    }}
    val transition = updateTransition(targetState = transitionState, label = "transition")
    val cardBgColor by transition.animateColor({
        tween(durationMillis = ExpandAnimation)
    }, label = "bgColorTransition") {
        if (expanded) md_theme_light_primary else md_theme_light_primary
    }
    val cardPaddingHorizontal by transition.animateDp({
        tween(durationMillis = ExpandAnimation)
    }, label = "paddingTransition") {
        20.dp
    }
    val cardElevation by transition.animateDp({
        tween(durationMillis = ExpandAnimation)
    }, label = "elevationTransition") {
        if (expanded) 20.dp else 5.dp
    }
    val cardRoundedCorners by transition.animateDp({
        tween(
            durationMillis = ExpandAnimation,
            easing = FastOutSlowInEasing
        )
    }, label = "cornersTransition") {
        15.dp
    }

    Card(
        backgroundColor = Color.LightGray,
        elevation = cardElevation,
        shape = RoundedCornerShape(cardRoundedCorners),
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = cardPaddingHorizontal,
                vertical = 8.dp
            )
            .clickable(
                interactionSource = interactionSource,
                indication = null
            ) {
                onClick()
            }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Box {
                Row(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Image(
                        modifier = Modifier
                            .size(60.dp)
                            .clip(RoundedCornerShape(topStart = 16.dp, bottomStart = 16.dp)),
                        painter = rememberImagePainter(
                            data = card.thumbnail,
                            builder = {
                                placeholder(R.drawable.placeholder)
                                error(R.drawable.placeholder)
                            }
                        ),
                        contentDescription = null,
                        contentScale = ContentScale.Crop
                    )

                    Column(
                        modifier = Modifier
                            .weight(0.85f)
                            .align(Alignment.CenterVertically)

                    ) {
                        Text(
                            text = card.customTranslation,
                            color = md_theme_light_onSurface,
                            textAlign = TextAlign.Start,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(12.dp)
                        )
                    }
                }
            }
            ExpandableContentLandmark(expanded,card) //que se pasa, bucar la cardDetail asociada al word,
            //o una función directa desde el viewmodel?
            //cambiar de card tipo word al detail tipo cardDetail
        }
    }
}

@ExperimentalAnimationApi
@Composable
fun ExpandableContentLandmark(expanded: Boolean = true, card: DictionaryWord) {

    val enterFadeIn = remember {
        fadeIn(
            animationSpec = TweenSpec(
                durationMillis = FadeInAnimation,
                easing = FastOutLinearInEasing
            )
        )
    }
    val enterExpand = remember {
        expandVertically(animationSpec = tween(ExpandAnimation))
    }
    val exitFadeOut = remember {
        fadeOut(
            animationSpec = TweenSpec(
                durationMillis = FadeOutAnimation,
                easing = LinearOutSlowInEasing
            )
        )
    }
    val exitCollapse = remember {
        shrinkVertically(animationSpec = tween(CollapseAnimation))
    }

    AnimatedVisibility(
        visible = expanded,
        enter = enterExpand + enterFadeIn,
        exit = exitCollapse + exitFadeOut
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color(217, 246, 255))
                .padding(8.dp)
        ) {
            Text(
                //usar nuevo objeto de detail
                text = card.customTranslation,
                textAlign = TextAlign.Left,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
                color = md_theme_light_onSurface,
                style = MaterialTheme.typography.bodyLarge
            )
            Text(
                //usar llamado a VM con función que retorne el detail
                text = card.customTranslation,
                textAlign = TextAlign.Left,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
                color = md_theme_light_onSurface,
                style = MaterialTheme.typography.bodyMedium

            )
            Image(
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(10f / 3f),
                painter = rememberImagePainter(
                    //usar llamado a VM con función que retorne el detail
                    data = card.thumbnail,
                    builder = {
                        placeholder(R.drawable.placeholder)
                        error(R.drawable.placeholder)
                    }
                ),
                contentDescription = null,
                contentScale = ContentScale.FillWidth
            )
            Text(
                //usar la descripción que está en translationsDescription en db
                //usar llamado a VM con función que retorne el detail
                text = card.customTranslation,
                textAlign = TextAlign.Left,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
                color = md_theme_light_onSurface
            )

            Row(
                horizontalArrangement = Arrangement.End
            ) {

                androidx.compose.material3.OutlinedButton(
                    onClick = {  },
                    modifier = Modifier.size(64.dp, 40.dp)
                ) {
                    androidx.compose.material3.Icon(
                        painter = painterResource(R.drawable.ic_volume_24),
                        contentDescription = null
                    )
                }
                Spacer(modifier = Modifier.padding(start = 30.dp))
                FilledTonalButton(
                    onClick = {  },
                    modifier = Modifier.size(175.dp, 35.dp)
                ) {
                    androidx.compose.material3.Text(text = "Add to flashcard")
                }
            }
        }
    }
}
