package com.moviles.langapp.utils

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences

class SharedPref private constructor() {

    companion object {
        private val sharePref = SharedPref()
        private lateinit var sharedPreferences: SharedPreferences

        fun getInstance(context: Context): SharedPref {
            if (!::sharedPreferences.isInitialized) {
                synchronized(SharedPref::class.java) {
                    if (!::sharedPreferences.isInitialized) {
                        sharedPreferences = context.getSharedPreferences(context.packageName, Activity.MODE_PRIVATE)
                    }
                }
            }
            return sharePref
        }
    }

    fun getVal(key: String) : String?{
        return sharedPreferences.getString(key,"")
    }

    fun save(key: String, value:String) {
        sharedPreferences.edit()
            .putString(key, value)
            .apply()
    }

    fun remove(key:String) {
        sharedPreferences.edit().remove(key).apply()
    }

    fun clearAll() {
        sharedPreferences.edit().clear().apply()
    }

}