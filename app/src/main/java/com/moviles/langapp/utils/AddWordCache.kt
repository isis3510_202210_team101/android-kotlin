package com.moviles.langapp.utils

import androidx.collection.ArrayMap

class AddWordCache {
    private object HOLDER {
        val INSTANCE = AddWordCache()
    }

    companion object {
        val instance: AddWordCache by lazy { AddWordCache.HOLDER.INSTANCE }
    }

    val cache: ArrayMap<String, String>

    init {
        cache = ArrayMap()
    }

    fun save_in_cache(key: String, value: String) {
        try {
            AddWordCache.instance.cache.put(key, value)
        } catch (e: Exception) {
        }
    }

    fun get_from_cache(key: String): String {
        try {
            return AddWordCache.instance.cache.get(key) ?: ""
        } catch (e: Exception) {
            return e.toString()
        }
    }

    fun clear_cache() {
        AddWordCache.instance.cache.clear()
    }
}