package com.moviles.langapp.utils

import androidx.collection.LruCache

class OnboardingCache private constructor() {

    private object HOLDER {
        val INSTANCE = OnboardingCache()
    }

    companion object {
        val instance: OnboardingCache by lazy { HOLDER.INSTANCE }
    }

    val lru: LruCache<String, String>

    init {
        lru = LruCache(20)
    }

    fun save_in_cache(key: String, value: String) {
        try {
            SignUpCache.instance.lru.put(key, value)
        } catch (e: Exception) {
        }
    }

    fun get_from_cache(key: String): String {
        try {
            return SignUpCache.instance.lru.get(key) ?: ""
        } catch (e: Exception) {
            return e.toString()
        }
    }

    fun clear_cache() {
        SignUpCache.instance.lru.evictAll()
    }

}