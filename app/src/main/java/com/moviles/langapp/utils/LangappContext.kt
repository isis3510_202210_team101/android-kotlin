package com.moviles.langapp.utils

import android.content.Context

abstract class LangappContext {

    companion object {

        private lateinit var context: Context

        fun setContext(con: Context) {
            context=con
        }

        fun getContext():Context{
            return  context
        }
    }
}