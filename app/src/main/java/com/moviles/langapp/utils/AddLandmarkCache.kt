package com.moviles.langapp.utils

import androidx.collection.ArrayMap
import androidx.collection.LruCache

class AddLandmarkCache {
    private object HOLDER{
        val INSTANCE = AddLandmarkCache()
    }
    companion object{
        val instance: AddLandmarkCache by lazy { AddLandmarkCache.HOLDER.INSTANCE }
    }
    val cache: ArrayMap<String, String>
    init {
        cache = ArrayMap()
    }
    fun save_in_cache(key:String, value:String){
        try {
            AddLandmarkCache.instance.cache.put(key,value)
        }catch (e:Exception){
        }
    }
    fun get_from_cache(key: String):String{
        try{
            return AddLandmarkCache.instance.cache.get(key)?:""
        }catch (e:Exception){
            return e.toString()
        }
    }
    fun clear_cache(){
        AddLandmarkCache.instance.cache.clear()
    }
}