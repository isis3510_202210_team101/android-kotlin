package com.moviles.langapp.viewmodels

import android.os.Handler
import android.os.Looper
import android.util.LruCache
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.moviles.langapp.model.DetailWord
import com.moviles.langapp.model.DictionaryWord
import com.moviles.langapp.provider.FirestorePagingSource
import com.moviles.langapp.utils.LangappContext
import com.moviles.langapp.utils.OnboardingCache
import com.moviles.langapp.utils.SharedPref
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.time.Duration.Companion.seconds

@HiltViewModel
class DictionaryViewModel @Inject constructor(
    private val queryWords: Query
) : ViewModel() {

    private val repoScope = CoroutineScope(Dispatchers.IO + SupervisorJob())

    val flow = Pager(
        PagingConfig(pageSize = 50)
    ) {
        FirestorePagingSource(queryWords)
    }.flow.cachedIn(repoScope)

    private val _dialogMessage = MutableLiveData<String>()

    val dialogMessage: LiveData<String>
        get() = _dialogMessage

    val lruCache: LruCache<String, DetailWord> = LruCache(50)


    fun fetchWordDetail(
        dictionaryWord: DictionaryWord?,
        word: String,
        isConnected: Boolean,
        fetched: () -> Unit,
        loading: () -> Unit
    ) {

        if (dictionaryWord == null) {
            _dialogMessage.value = "No connection"
            _dialogMessage.value = ""
            return
        }
        if (!dictionaryWord.detailWord?.description.isNullOrEmpty() || !dictionaryWord.detailWord?.translation.isNullOrEmpty() || !dictionaryWord.detailWord?.photo.isNullOrEmpty()) {
            fetched()
            loading()
            return
        }
        if (word == "") {
            _dialogMessage.value = "There was an error fetching the word detail"
            _dialogMessage.value = ""
            return
        }
        val context = LangappContext.getContext()
        val prefs = SharedPref.getInstance(context)

        val learningLanguage = prefs.getVal("LEARNINGLANGUAGE")!!.ifEmpty {
            "Spanish"
        }
        if (!isConnected) {
            _dialogMessage.value = "No connection"
            _dialogMessage.value = ""
        }
        viewModelScope.launch(Dispatchers.IO) {
            withTimeout(5.seconds) {
                try {
                    val translation = Tasks.await(
                        FirebaseFirestore.getInstance().collection("translations")
                            .document("${learningLanguage}-${word}").get()
                    )
                    val description = Tasks.await(
                        FirebaseFirestore.getInstance().collection("translationsDescription")
                            .document("${learningLanguage}-${word}").get()
                    )
                    val photo = if (isConnected) {
                        Tasks.await(
                            FirebaseFirestore.getInstance()
                                .collection("users/${FirebaseAuth.getInstance().currentUser?.email}/dictionaryPhotos")
                                .document("${FirebaseAuth.getInstance().currentUser?.email}-${word}")
                                .get()
                        ).get("photo")
                    } else ""
                    if (translation != null && description != null) {
                        val det = DetailWord(
                            description.get("description") as String,
                            photo as String,
                            translation.get("translation") as String
                        )
                        dictionaryWord.detailWord = det
                        lruCache.put(word, det)
                    }
                    fetched()
                    loading()

                } catch (e: Exception) {
                    _dialogMessage.postValue("There was an error retrieving the word. Check your connectivity and try again")
                    Handler(Looper.getMainLooper()).postDelayed({
                        _dialogMessage.postValue("")

                    }, 2000)
                    loading()
                }
            }
        }
    }

}