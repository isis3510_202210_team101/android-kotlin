package com.moviles.langapp.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.moviles.langapp.model.Languages
import com.moviles.langapp.model.Words
import com.moviles.langapp.utils.LangappContext
import com.moviles.langapp.utils.OnboardingCache
import com.moviles.langapp.utils.SharedPref
import com.moviles.langapp.utils.SignUpCache
import com.moviles.langapp.views.preferredLanguageUserGlobal
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OnboardingVM @Inject constructor(
    //private val repository: ExamplesRepository
) : ViewModel() {
    private val _languages = MutableLiveData<List<Languages>>()
    val languages: LiveData<List<Languages>>
        get() = _languages

    val db = Firebase.firestore

    val langs = db.collection("languages")


    fun getLanguages1(): LiveData<List<Languages>> {
        viewModelScope.launch(Dispatchers.IO) {
            val languageList = arrayListOf<Languages>()
            langs.get().addOnSuccessListener { documents ->
                for (document in documents) {
                    val flag: String = document.get("flag") as String
                    val language: String = document.get("language") as String
                    languageList.add(Languages(language = language, flag = flag))
                    _languages.value = languageList
                }
            }.addOnFailureListener {

            }
        }
        return _languages
    }

    val auth = FirebaseAuth.getInstance()
    var cache = OnboardingCache.instance
    fun setLearningLanguages(languages: List<Languages>) {

        val elements = arrayListOf<DocumentReference>()
        preferredLanguageUserGlobal = languages[0].language
        for (language in languages) {
            var new = ""
            when (language.language) {
                "English" -> {
                    new = "English"
                }
                "Deutsch" -> {
                    new = "German"
                }
                "Italiano" -> {
                    new = "Italian"
                }
                "Português" -> {
                    new = "Portuguese"
                }
                "Español" -> {
                    new = "Spanish"
                }
            }
            elements.add(db.document("languages/${new}"))
        }

        //save learning languages in cache
        cache.save_in_cache("LEARNING", elements.toString())

        val email: String = auth.currentUser?.email ?: throw Exception("user not logged in")

        if (auth.currentUser != null) {
            db.collection("users")
                .document(email)
                .update("learnedLanguages", elements)
                .addOnSuccessListener { }
                .addOnFailureListener {}
        }
    }


    private val _success = MutableLiveData<Boolean>()
    val success: LiveData<Boolean>
        get() = _success
    var userId: String = ""
    private val _dialogMessage = MutableLiveData<String>()

    fun createUser_back(
        birthDate: String,
        email: String,
        lastname: String,
        name: String,
        residenceCountry: String,
        nativeLanguage: DocumentReference,
        learnedLanguages: List<DocumentReference>,
        duolingo: Boolean

    ): String {
        val values = hashMapOf<String, Any>(
            "name" to name,
            "lastname" to lastname,
            "birthDate" to birthDate,
            "email" to email,
            "residenceCountry" to residenceCountry,
            "learnedLanguages" to learnedLanguages,
            "nativeLanguage" to nativeLanguage,
            "visitedLandmarks" to "",
            "duolingo" to duolingo
        )
        var rta: String = ""
        var db: DocumentReference = FirebaseFirestore.getInstance().document("users/$email")
        db.set(values).addOnSuccessListener {
            rta = "Correct"
            SignUpCache.instance.clear_cache()
        }.addOnFailureListener {
            rta = "Incorrect"
        }
        Log.d("SIGN_UP", rta)
        return rta
    }

    fun mapToDefaultLanguage(language: Languages): String {
        return when (language.language) {
            "English" -> {
                "English"
            }
            "Deutsch" -> {
                "German"
            }
            "Italiano" -> {
                "Italian"
            }
            "Português" -> {
                "Portuguese"
            }
            else -> {
                "Spanish"
            }
        }
    }

    fun createUser (languages: List<Languages>){
        val context = LangappContext.getContext()
        val prefs = SharedPref.getInstance(context)

        val elements = arrayListOf<DocumentReference>()
        val newLanguages = languages.map { mapToDefaultLanguage(it) }
        OnboardingCache.instance.save_in_cache("LEARNINGLANGUAGES", newLanguages.toString())
        OnboardingCache.instance.save_in_cache("LEARNINGLANGUAGE", newLanguages[0])
        prefs.save("LEARNINGLANGUAGE",newLanguages[0])

        for (language in languages) {
            var new = ""
            when (language.language) {
                "English" -> {
                    new = "English"
                }
                "Deutsch" -> {
                    new = "German"
                }
                "Italiano" -> {
                    new = "Italian"
                }
                "Português" -> {
                    new = "Portuguese"
                }
                "Español" -> {
                    new = "Spanish"
                }
            }
            elements.add(db.document("languages/${new}"))
        }


        val birthdate = OnboardingCache.instance.get_from_cache("BIRTHDATE")
        val email = OnboardingCache.instance.get_from_cache("EMAIL")
        val lastname = OnboardingCache.instance.get_from_cache("LASTNAME")
        val name = OnboardingCache.instance.get_from_cache("NAME")
        val residenceCountry = OnboardingCache.instance.get_from_cache("RESIDENCE")
        var nt = OnboardingCache.instance.get_from_cache("NATIVE")
        val duo = OnboardingCache.instance.get_from_cache("DUOLINGO")
        val password = OnboardingCache.instance.get_from_cache("PASSWORD")

        var natLang = db.document("languages/" + nt)

        var duolingo = duo == "1"

        _dialogMessage.value = ""
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val user = auth.currentUser
                    if (user != null)
                        userId = user.uid
                    _success.value = true
                } else {
                    _success.value = false
                }
            }.addOnFailureListener {
                _dialogMessage.value = it.message.toString()
            }
        val rta = createUser_back(
            birthDate = birthdate,
            email = email,
            lastname = lastname,
            name = name,
            residenceCountry = residenceCountry,
            nativeLanguage = natLang,
            learnedLanguages = elements,
            duolingo = duolingo

        )
        if (rta == "Correct") {
            _dialogMessage.value = "User registered correctly"
        } else {
            _dialogMessage.value = rta
        }
    }
}