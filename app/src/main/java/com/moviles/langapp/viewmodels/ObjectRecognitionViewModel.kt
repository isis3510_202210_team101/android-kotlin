package com.moviles.langapp.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.moviles.langapp.model.PredictionsDao
import com.moviles.langapp.repository.ObjectRegnitionRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ObjectRecognitionViewModel @Inject constructor(
    private val auth: FirebaseAuth,
    private val repository: ObjectRegnitionRepository
) : ViewModel() {

    val _predictions = MutableLiveData<List<PredictionsDao>>()

    fun resetLabels() {
        _predictions.value = emptyList()
    }

    fun getImageLabels(
        path: String,
        native: String,
        objective: String
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            var labels = emptyList<PredictionsDao>()
            labels = repository.getImageLabels(path, native, objective)
            _predictions.postValue(labels)
        }
    }
}