package com.moviles.langapp.viewmodels

data class LoginState(
    val email: String = "",
    val password: String = "",
    val successLogin: Boolean = false
)