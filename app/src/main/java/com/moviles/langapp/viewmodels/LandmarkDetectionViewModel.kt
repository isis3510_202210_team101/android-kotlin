package com.moviles.langapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.moviles.langapp.model.LandmarksDao
import com.moviles.langapp.model.PredictionsDao
import com.moviles.langapp.repository.LandmarkDetectionRepository
import com.moviles.langapp.repository.ObjectRegnitionRepository
import com.moviles.langapp.views.termino_Land
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LandmarkDetectionViewModel @Inject constructor(
    private val auth:FirebaseAuth,
    private val repository: LandmarkDetectionRepository
): ViewModel(){

    val _predictions = MutableLiveData<List<LandmarksDao>>()

    fun resetLabels(){
        _predictions.value = emptyList()
    }

    fun getLandmark(
    path: String
    ){
        viewModelScope.launch (Dispatchers.Main) {
            val user: String? = auth.currentUser?.email
            var labels =  emptyList<LandmarksDao>()
            if (user != null){
                try{
                    labels = repository.addLandmark(path, user)
                }catch (e:NoUserException){

                }
            }
            _predictions.postValue(labels)
            termino_Land = true
        }
    }
}