package com.moviles.langapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.moviles.langapp.repository.HomeRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddWordScreenViewModel @Inject constructor(
    private val repository: HomeRepository
) : ViewModel() {

    private val auth = FirebaseAuth.getInstance()


    fun addWord(
        latitude: Double,
        longitude: Double,
        word: String,
        learningLanguage: String,
        photo: String
    ) {
        viewModelScope.launch {
            val email: String = auth.currentUser?.email ?: throw NoUserException()
            repository.addWord(
                user = email,
                latitude = latitude,
                longitude = longitude,
                photo,
                learningLanguage,
                word
            )
        }
    }
}

class NoUserException : Exception()