package com.moviles.langapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.moviles.langapp.model.Languages
import com.moviles.langapp.model.Words
import com.moviles.langapp.utils.AddWordCache
import com.moviles.langapp.utils.LangappContext
import com.moviles.langapp.utils.OnboardingCache
import com.moviles.langapp.utils.SharedPref
import com.moviles.langapp.views.nativeLanguageUserGlobal
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OnboardingNatVM @Inject constructor(
    //private val repository: ExamplesRepository
) : ViewModel() {
    private val _languages = MutableLiveData<List<Languages>>()
    val languages: LiveData<List<Languages>>
        get() = _languages

    val db = Firebase.firestore

    val langs = db.collection("languages")

    fun getLanguages1(): LiveData<List<Languages>> {
        viewModelScope.launch(Dispatchers.IO) {
            val languageList = arrayListOf<Languages>()
            langs.get().addOnSuccessListener { documents ->
                for (document in documents) {
                    val flag: String = document.get("flag") as String
                    val language: String = document.get("language") as String
                    languageList.add(Languages(language = language, flag = flag))
                }
                _languages.postValue(languageList)
            }.addOnFailureListener {

            }
        }
        return _languages
    }

    var cache = OnboardingCache.instance
    var selected = ""
    val auth = FirebaseAuth.getInstance()
    val context = LangappContext.getContext()
    val prefs = SharedPref.getInstance(context)


    fun setS(str: String) {
        selected = str
        when (selected) {
            "English" -> {
                selected = "English"
            }
            "Deutsch" -> {
                selected = "German"
            }
            "Italiano" -> {
                selected = "Italian"
            }
            "Português" -> {
                selected = "Portuguese"
            }
            "Español" -> {
                selected = "Spanish"
            }
        }
        //guardar native en cache
        cache.save_in_cache("NATIVE",str)
        prefs.save("NATIVE",str)

        nativeLanguageUserGlobal = selected

    }
}