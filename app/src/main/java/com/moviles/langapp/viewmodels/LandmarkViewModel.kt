package com.moviles.langapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.moviles.langapp.model.Landmark
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class LandmarkViewModel: ViewModel() {

    private val _landMarks = MutableStateFlow(listOf<Landmark>())
    val landmarks: StateFlow<List<Landmark>> get() = _landMarks



    val queryGetVisitedLandmarks: Query =
        FirebaseFirestore
            .getInstance().collection("users/${FirebaseAuth
                .getInstance().currentUser?.email}/visitedLandmarks")

    fun getUserLandrmarks(){

        viewModelScope.launch(Dispatchers.IO) {
            val visitedLandmarks: QuerySnapshot = Tasks.await(queryGetVisitedLandmarks.get())
            val landmarks: List<Landmark> = visitedLandmarks.documents.map {
                val landmarkName = it.id.split("-")[1]
                val detail = Tasks.await(FirebaseFirestore.getInstance().collection("landmarks").document(landmarkName).get())
                val description = if(detail.get("description") != null){
                    detail.get("description") as String
                }else{
                    ""
                }
                val originalName = if(detail.get("originalName") != null){
                    detail.get("originalName") as String
                }else{
                    landmarkName
                }
                val photo = if(it.get("photo") != null){
                    it.get("photo") as String
                }else{
                    ""
                }
                Landmark(
                    description = description,
                    originalName = originalName,
                    photo = photo,
                    translatedName = landmarkName
                )
            }
            _landMarks.emit(landmarks)
        }

    }
}