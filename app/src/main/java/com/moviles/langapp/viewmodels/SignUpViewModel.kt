package com.moviles.langapp.viewmodels

import android.util.Log
import android.util.LruCache
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.moviles.langapp.utils.OnboardingCache
import com.moviles.langapp.utils.SignUpCache
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

private val cacheSignUp = LruCache<String, String>(1000)

@HiltViewModel
class SignUpViewModel @Inject constructor(
) : ViewModel() {
    private val _success = MutableLiveData<Boolean>()
    val success: LiveData<Boolean>
        get() = _success
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()
    val _dialogMessage = MutableLiveData<String>()

    val dialogMessage: LiveData<String>
        get() = _dialogMessage

    fun save_in_cache(key: String, value: String) {
        cacheSignUp.put(key, value)
    }

    fun get_from_cache(key: String): String {
        Log.d("DEBUG_DENTRO", cacheSignUp.get(key) ?: "Error")
        return cacheSignUp.get(key) ?: ""
    }

    fun createUser(
        email: String,
        password: String,
        birthdate: String,
        lastname: String,
        name: String,
        residenceCountry: String,
        duolingo: String
    ) {
        _dialogMessage.value = ""
        viewModelScope.launch(Dispatchers.Main) {
            if (email == "" || password == "") {
                _dialogMessage.value = "Email or password empty."
            } else if (birthdate == "" || lastname == "" || name == "" || residenceCountry == "") {
                _dialogMessage.value = "Please complete all the fields."
            } else {
                OnboardingCache.instance.save_in_cache("NAME", name)
                OnboardingCache.instance.save_in_cache("LASTNAME", lastname)
                OnboardingCache.instance.save_in_cache("EMAIL", email)
                OnboardingCache.instance.save_in_cache("BIRTHDATE", birthdate)
                OnboardingCache.instance.save_in_cache(
                    "PASSWORD",
                    password
                )       //se debería encriptar
                OnboardingCache.instance.save_in_cache("RESIDENCE", residenceCountry)
                OnboardingCache.instance.save_in_cache("DUOLINGO", duolingo)

            }
        }
    }

}