package com.moviles.langapp.viewmodels

import androidx.lifecycle.MutableLiveData
import com.moviles.langapp.repository.HomeRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.moviles.langapp.model.LandmarksDao
import kotlinx.coroutines.launch
import java.lang.Exception
import com.moviles.langapp.repository.LandmarkDetectionRepository
import kotlinx.coroutines.Dispatchers

@HiltViewModel
class AddLandmarkScreenViewModel @Inject constructor(
    private val repository: LandmarkDetectionRepository): ViewModel(){
    val _landmark = MutableLiveData<List<LandmarksDao>>()
    private val auth = FirebaseAuth.getInstance()

    fun addLandMark(photo: String){
        viewModelScope.launch (Dispatchers.Main){
            var landmark = emptyList<LandmarksDao>()
            val user:String = auth.currentUser?.email?: throw NoUserException()
            landmark = repository.addLandmark(path = photo, user=user)
            _landmark.postValue(landmark)
        }
    }
}