package com.moviles.langapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.moviles.langapp.model.Languages
import com.moviles.langapp.utils.LangappContext
import com.moviles.langapp.utils.SharedPref
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginScreenViewModel @Inject constructor(
    private var auth: FirebaseAuth
) : ViewModel() {


    var userId: String = ""

    private val _success = MutableLiveData<Boolean>()
    val success: LiveData<Boolean>
        get() = _success
    private val _dialogMessage = MutableLiveData<String>()

    val dialogMessage: LiveData<String>
        get() = _dialogMessage

    val db = Firebase.firestore

    fun login(email: String, password: String) {
        val context = LangappContext.getContext()
        val prefs = SharedPref.getInstance(context)
        _dialogMessage.value = ""
        viewModelScope.launch(Dispatchers.Main) {
            if (email == "" || password == "") {
                _dialogMessage.value = "Email or password empty."
            } else {
                auth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
                    if (it.isSuccessful) {
                        val user = auth.currentUser
                        if (user != null)
                            userId = user.uid
                        _success.value = true
                        val langs = db.document("users/${FirebaseAuth.getInstance().currentUser?.email}")
                        var lista: List<DocumentReference>
                        langs.get().addOnSuccessListener { doc ->
                            val nat = doc.get("nativeLanguage") as DocumentReference
                            lista = doc.get("learnedLanguages") as List<DocumentReference>
                            var o = lista.map { it.id }
                            prefs.save("LEARNINGLANGUAGE",o[0])
                            prefs.save("NATIVE",nat.id)
                        }.addOnFailureListener {}
                    } else {
                        _success.value = false
                    }
                }.addOnFailureListener {
                    _dialogMessage.value = it.message
                }
            }
        }

    }
}

