package com.moviles.langapp.viewmodels

import androidx.compose.runtime.Composable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldPath
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.moviles.langapp.model.Languages
import com.moviles.langapp.utils.OnboardingCache
import com.moviles.langapp.views.nativeLanguageUserGlobal
import dagger.hilt.android.internal.earlyentrypoint.AggregatedEarlyEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SelectLearningLangVM @Inject constructor(
): ViewModel() {
    private val _learning = MutableLiveData<List<Languages>>()
    val learning : LiveData<List<Languages>>
        get() = _learning

    val db = Firebase.firestore

    val langs = db.document("users/${FirebaseAuth.getInstance().currentUser?.email}")


    fun getLearningLangs() :LiveData<List<Languages>>{
        viewModelScope.launch(Dispatchers.IO) {
            var lista: List<DocumentReference> = emptyList()
            val languageList = arrayListOf<Languages>()
            var otra = arrayListOf<String>()

            langs.get().addOnSuccessListener { doc ->
                lista = doc.get("learnedLanguages") as List<DocumentReference>
                var otra = lista.map { it.id }
                db.collection("languages").whereIn(FieldPath.documentId(),otra).get().addOnSuccessListener { documents ->
                    for(document in documents){
                        val flag:String = document.get("flag") as String
                        val language:String = document.get("language") as String
                        languageList.add(Languages(language = language,flag = flag))
                    }
                    _learning.postValue(languageList)
                }.addOnFailureListener{}
            }.addOnFailureListener {}

        }
        return _learning
    }
}