package com.moviles.langapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.moviles.langapp.model.Examples
import com.moviles.langapp.repository.ExamplesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListExampleScreenViewModel @Inject constructor(
    private val repository: ExamplesRepository
) : ViewModel() {
    private val _examples = MutableLiveData<List<Examples>>()

    fun getExamples(): LiveData<List<Examples>> {
        viewModelScope.launch(Dispatchers.IO) {
            val news = repository.getExamples("US")
            _examples.postValue(news)
        }
        return _examples
    }
}