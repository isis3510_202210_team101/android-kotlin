package com.moviles.langapp.viewmodels

import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.moviles.langapp.model.Proportions
import com.moviles.langapp.model.WordResponseDao
import com.moviles.langapp.repository.HomeRepository
import com.moviles.langapp.repository.NetworkManager
import com.moviles.langapp.repository.NoWordsCachedException
import com.moviles.langapp.repository.cache.NearestWordsDataStore
import com.moviles.langapp.utils.LangappContext
import com.moviles.langapp.utils.OnboardingCache
import com.moviles.langapp.utils.SharedPref
import com.moviles.langapp.utils.SignUpCache
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(

    private val repository: HomeRepository,
    private val auth: FirebaseAuth
) : ViewModel() {

    val _proportions = MutableLiveData<Proportions>()
    val _forgotten = MutableLiveData<List<String>>()
    val _words = MutableLiveData<List<WordResponseDao>>()
    val _pop = MutableLiveData<String>()


    fun getNearestWords(
        latitude: Double,
        longitude: Double,
        isConnected: Boolean,
        dataStore: NearestWordsDataStore
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val email: String? = auth.currentUser?.email
            var words = emptyList<WordResponseDao>()
            if (email != null) {
                try {
                    words = repository.getNearestWords(
                        email,
                        latitude,
                        longitude,
                        isConnected,
                        dataStore
                    )
                } catch (e: NoWordsCachedException) {

                }
            }
            _words.postValue(words)
        }
    }

    fun getProportions(context: Context) {
        viewModelScope.launch(Dispatchers.IO) {

            val email: String? = auth.currentUser?.email
            var proportions = Proportions()
            if (email != null) {
                try {
                    if (NetworkManager.checkForInternet(context)) {
                        proportions = repository.langProportions(email)
                    }
                } catch (e: NoWordsCachedException) {

                }
            }
            _proportions.postValue(proportions)
        }
    }

    fun getForgotten(context: Context) {
        viewModelScope.launch(Dispatchers.IO) {
            val email: String? = auth.currentUser?.email
            var forgotten = emptyList<String>()
            if (email != null) {
                try {
                    if (NetworkManager.checkForInternet(context)) {
                        forgotten = repository.getForgotten(email)
                    }
                } catch (e: NoWordsCachedException) {

                }
            }
            _forgotten.postValue(forgotten)
        }
    }


    val signUpCache = SignUpCache.instance
    val onboardCache = OnboardingCache.instance
    val context = LangappContext.getContext()
    val prefs = SharedPref.getInstance(context)

    fun logOut() {
        signUpCache.clear_cache()
        onboardCache.clear_cache()
        prefs.clearAll()
        auth.signOut()
    }

    val db = Firebase.firestore
    val landStat = db.collection("statisticsLandmarks").document("popular")

    fun getPopular(context: Context) {
        viewModelScope.launch (Dispatchers.IO) {
            var pop :String = ""
                try {
                    if(NetworkManager.checkForInternet(context)){
                        landStat.get().addOnSuccessListener { doc ->
                            if (doc != null){
                                var refPop:DocumentReference = doc.get("first") as DocumentReference
                                pop = refPop.id.split("/").last()
                            }
                            else {
                                Log.d(TAG, "No such document")
                            }
                            _pop.postValue(pop)
                        }
                    }
                }catch (e: NoWordsCachedException){
            }
        }
    }

    fun checkPreferences() {
        val context = LangappContext.getContext()
        val prefs = SharedPref.getInstance(context)
        if (prefs.getVal("NATIVE")!!.isEmpty() || prefs.getVal("LEARNINGLANGUAGE")!!.isEmpty()){
            viewModelScope.launch(Dispatchers.IO) {
                val langs = db.document("users/${FirebaseAuth.getInstance().currentUser?.email}")
                var lista: List<DocumentReference>
                langs.get().addOnSuccessListener { doc ->
                    val nat = doc.get("nativeLanguage") as DocumentReference
                    lista = doc.get("learnedLanguages") as List<DocumentReference>
                    var o = lista.map { it.id }
                    prefs.save("LEARNINGLANGUAGE",o[0])
                    prefs.save("NATIVE",nat.id)
                }.addOnFailureListener {}
            }
        }
    }

}