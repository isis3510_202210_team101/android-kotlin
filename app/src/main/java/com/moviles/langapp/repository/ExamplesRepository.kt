package com.moviles.langapp.repository

import com.moviles.langapp.model.Examples
import com.moviles.langapp.provider.ExamplesProvider
import javax.inject.Inject

interface ExamplesRepository {

    suspend fun getExamples(country: String): List<Examples>
    fun getExample(title: String): Examples
}

class ExamplesRepositoryImp @Inject constructor(
    private val newsProvider: ExamplesProvider
) : ExamplesRepository {

    private var news: List<Examples> = emptyList()

    override fun getExample(title: String): Examples =
        news.first { it.title == title }

    override suspend fun getExamples(country: String): List<Examples> {
        val apiResponse = newsProvider.topHeadlines(country).body()
        if (apiResponse?.status == "error") {
            when (apiResponse.code) {
                "apiKeyMissing" -> throw MissingApiKeyException()
                "apiKeyInvalid" -> throw ApikeyInvalidException()
                else -> throw Exception()
            }
        }
        news = apiResponse?.articles ?: emptyList()
        return news
    }
}

class MissingApiKeyException : java.lang.Exception()
class ApikeyInvalidException : java.lang.Exception()