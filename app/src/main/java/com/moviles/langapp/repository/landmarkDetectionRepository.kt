package com.moviles.langapp.repository

import com.moviles.langapp.model.PredictionsDao
import com.moviles.langapp.provider.ObjectRecognitionProvider
import javax.inject.Inject
import android.util.Log
import androidx.compose.ui.platform.LocalContext
import com.moviles.langapp.model.LandmarksDao
import com.moviles.langapp.provider.LandmarkDetectionProvider
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody

interface LandmarkDetectionRepository{
    suspend fun addLandmark(path:String, user:String): List<LandmarksDao>
}

class LandmarkDetectionRepositoryImp @Inject constructor(
    private val landmarkDetectionProvider:LandmarkDetectionProvider
): LandmarkDetectionRepository{
    private var predictions: List<LandmarksDao> = emptyList()
    override suspend fun addLandmark(
        path: String,
        user:String
    ): List<LandmarksDao> {
        var body = "{\n" +
                "        \"image\": {\n" +
                "    \t\t\"source\": {\"imageUri\": \"$path\"}\n" +
                "\t\t},\n"+
                "        \"user\":\"$user\"\n"+
                "}"
        val requestBody = body.toRequestBody("application/json".toMediaTypeOrNull())
        val apiResponse = landmarkDetectionProvider.getLandmark(requestBody)
        val final_predictions: MutableList<LandmarksDao> = mutableListOf()
        Log.d("LANDMARK4", apiResponse.isSuccessful.toString())
        if(apiResponse.isSuccessful) {
            predictions = apiResponse.body() ?: emptyList()
            final_predictions += predictions
            Log.d("LANDMARK4", final_predictions.toString())
            return final_predictions
        }else{
            return emptyList()
        }
    }
}