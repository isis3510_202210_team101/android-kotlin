package com.moviles.langapp.repository

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.moviles.langapp.model.*
import com.moviles.langapp.provider.HomeProvider
import com.moviles.langapp.repository.cache.NearestWordsDataStore
import kotlinx.coroutines.flow.first
import javax.inject.Inject


interface HomeRepository {

    suspend fun getNearestWords(
        user: String,
        latitude: Double,
        longitude: Double,
        isConnected: Boolean,
        dataStore: NearestWordsDataStore
    ): List<WordResponseDao>

    suspend fun addWord(
        user: String,
        latitude: Double,
        longitude: Double,
        photo: String?,
        learningLanguage: String,
        word: String
    )

    suspend fun langProportions(user: String): Proportions

    suspend fun getForgotten(user: String): List<String>
}

class HomeRepositoryImp @Inject constructor(
    private val homeProvider: HomeProvider
) : HomeRepository {

    private var words: List<WordResponseDao> = emptyList()

    private suspend fun getNearestWordsBack(
        user: String,
        latitude: Double,
        longitude: Double,
        dataStore: NearestWordsDataStore
    ): List<WordResponseDao> {
        try {
            val apiResponse =
                homeProvider.getNearestWords(
                    userNearestDao = UserNearestDao(
                        user,
                        latitude,
                        longitude
                    )
                )
            words = apiResponse.body() ?: emptyList()
            if (words.isNotEmpty()) {
                cacheWords(words, latitude, longitude, dataStore)
            }
        } catch (e: Exception) {
            Firebase.crashlytics.recordException(e)
        }

        return words
    }

    private suspend fun cacheWords(
        words: List<WordResponseDao>,
        latitude: Double,
        longitude: Double,
        dataStore: NearestWordsDataStore
    ) {

        dataStore.saveWords(words)
        dataStore.saveLatitude(latitude)
        dataStore.saveLongitude(longitude)
    }

    private suspend fun getNearestWordsCache(
        latitude: Double,
        longitude: Double,
        dataStore: NearestWordsDataStore
    ): List<WordResponseDao> {
        try {
            if (latitude == dataStore.getLatitude.first() && longitude == dataStore.getLongitude.first()) {
                return dataStore.getWords.first()?.split(",")?.map { WordResponseDao(it) }
                    ?: throw NoWordsCachedException()
            }
        } catch (e: Exception) {
            Firebase.crashlytics.log("User ${FirebaseAuth.getInstance().currentUser?.email} had a crash while retrieving nearest words")
            Firebase.crashlytics.recordException(e)
        }

        return words
    }

    override suspend fun addWord(
        user: String,
        latitude: Double,
        longitude: Double,
        photo: String?,
        learningLanguage: String,
        word: String
    ) {
        homeProvider.addWord(
            AddWordRequestDao(
                user,
                latitude,
                longitude,
                learningLanguage,
                word,
                photo ?: ""
            )
        )

    }

    override suspend fun getNearestWords(
        user: String,
        latitude: Double,
        longitude: Double,
        isConnected: Boolean,
        dataStore: NearestWordsDataStore
    ): List<WordResponseDao> {
        return if (isConnected) {
            getNearestWordsBack(user, latitude, longitude, dataStore)
        } else {
            getNearestWordsCache(latitude, longitude, dataStore)
        }
    }

    private var props: Proportions = Proportions()

    override suspend fun langProportions(user: String): Proportions {
        val apiResponse = homeProvider.langProportions(UserProportions(user))
        props = apiResponse.body() ?: Proportions()
        println(apiResponse.body())
        return props
    }

    private var forgotten: List<String> = emptyList()

    override suspend fun getForgotten(user: String): List<String> {
        val apiResponse = homeProvider.getForgotten(UserProportions(user))
        forgotten = apiResponse.body() ?: emptyList()
        println(apiResponse.body())
        return forgotten
    }
}

class NoWordsCachedException : Exception()