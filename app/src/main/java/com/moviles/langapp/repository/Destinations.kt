package com.moviles.langapp.repository

object Destinations {
    const val AUTH_URL: String = "https://identitytoolkit.googleapis.com/v1/"
    const val NEWS_URL: String = "https://newsapi.org/v2/"
}