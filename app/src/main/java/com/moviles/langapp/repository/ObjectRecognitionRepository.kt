package com.moviles.langapp.repository

import com.moviles.langapp.model.PredictionsDao
import com.moviles.langapp.provider.ObjectRecognitionProvider
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import javax.inject.Inject

interface ObjectRegnitionRepository {
    suspend fun getImageLabels(
        path: String,
        native: String,
        objective: String
    ): List<PredictionsDao>
}

class ObjectRecognitionRepositoryImp @Inject constructor(
    private val ObjectRecognitionProvider: ObjectRecognitionProvider
) : ObjectRegnitionRepository {
    private var predictions: List<PredictionsDao> = emptyList()
    override suspend fun getImageLabels(
        path: String,
        native: String,
        objective: String
    ): List<PredictionsDao> {
        var body = "{\n" +
                "        \"image\": {\n" +
                "    \t\t\"source\": {\"imageUri\": \"$path\"}\n" +
                "\t\t},\n" +
                "        \"native\":\"$native\",\n" +
                "        \"objective\":\"$objective\"\n" +
                "}"
        val requestBody = body.toRequestBody("application/json".toMediaTypeOrNull())
        val apiResponse = ObjectRecognitionProvider.getImageLabels(requestBody)
        val final_predictions: MutableList<PredictionsDao> = mutableListOf()
        if (apiResponse.isSuccessful) {
            predictions = apiResponse.body() ?: emptyList()
            final_predictions += predictions
            final_predictions += PredictionsDao("", "None of the above")
            return final_predictions
        } else {
            return emptyList()
        }
    }
}