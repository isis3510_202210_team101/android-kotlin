package com.moviles.langapp.repository.cache

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.doublePreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.moviles.langapp.model.WordResponseDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


class NearestWordsDataStore(private val context: Context) {

    companion object {
        private val Context.dataStore: DataStore<Preferences> by preferencesDataStore("nearestWords")
        val LATITUDE_KEY = doublePreferencesKey("latitude")
        val LONGITUDE_KEY = doublePreferencesKey("longitude")
        val WORDS_KEY = stringPreferencesKey("words")
    }

    val getWords: Flow<String?> = context.dataStore.data
        .map { preferences ->
            preferences[WORDS_KEY] ?: ""
        }
    val getLatitude: Flow<Double?> = context.dataStore.data
        .map { preferences ->
            preferences[LATITUDE_KEY] ?: -1.0
        }
    val getLongitude: Flow<Double?> = context.dataStore.data
        .map { preferences ->
            preferences[LONGITUDE_KEY] ?: -1.0
        }

    suspend fun saveLatitude(name: Double) {
        context.dataStore.edit { preferences ->
            preferences[LATITUDE_KEY] = name
        }
    }

    suspend fun saveLongitude(name: Double) {
        context.dataStore.edit { preferences ->
            preferences[LONGITUDE_KEY] = name
        }
    }

    suspend fun saveWords(words: List<WordResponseDao>) {
        context.dataStore.edit { preferences ->
            preferences[WORDS_KEY] = words.joinToString(separator = ",") { it.word }
        }
    }

}