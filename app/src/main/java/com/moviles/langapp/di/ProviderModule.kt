package com.moviles.langapp.di

import com.moviles.langapp.provider.ExamplesProvider
import com.moviles.langapp.provider.HomeProvider
import com.moviles.langapp.provider.LandmarkDetectionProvider
import com.moviles.langapp.provider.ObjectRecognitionProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrl
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class ProviderModule {

    @Provides
    @Named("BaseUrl")
    fun provideBaseUrl() = "https://lang-app-moviles-t10.herokuapp.com/".toHttpUrl()

    @Singleton
    @Provides
    fun provideRetrofit(@Named("BaseUrl") baseUrl: HttpUrl): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .build()
    }

    @Provides
    @Singleton
    fun providerHomeProvider(retrofit: Retrofit): HomeProvider =
        retrofit.create(HomeProvider::class.java)

    @Provides
    @Singleton
    fun providerObjectRecognitionProvider(retrofit: Retrofit): ObjectRecognitionProvider =
        retrofit.create(ObjectRecognitionProvider::class.java)

    @Provides
    @Singleton
    fun providerLandmarkDetectionProvider(retrofit: Retrofit): LandmarkDetectionProvider=
        retrofit.create((LandmarkDetectionProvider::class.java))

    @Provides
    @Singleton
    fun providerExamplesProvider(retrofit: Retrofit): ExamplesProvider =
        retrofit.create(ExamplesProvider::class.java)

}