package com.moviles.langapp.di


import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.moviles.langapp.provider.ExamplesProvider
import com.moviles.langapp.provider.HomeProvider
import com.moviles.langapp.provider.ObjectRecognitionProvider
import com.moviles.langapp.provider.LandmarkDetectionProvider
import com.moviles.langapp.repository.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun providerNewsRepository(provider: ExamplesProvider): ExamplesRepository =
        ExamplesRepositoryImp(provider)

    @Provides
    @Singleton
    fun providerHomesRepository(provider: HomeProvider): HomeRepository =
        HomeRepositoryImp(provider)

    @Provides
    @Singleton
    fun providerObjectRecognitionRepository(provider: ObjectRecognitionProvider): ObjectRegnitionRepository =
        ObjectRecognitionRepositoryImp(provider)

    @Provides
    @Singleton
    fun providerLandmarkDetectionRepository(provider: LandmarkDetectionProvider): LandmarkDetectionRepository=
        LandmarkDetectionRepositoryImp(provider)

    @Provides
    fun provideAuthentication(): FirebaseAuth = FirebaseAuth.getInstance()

    @Provides
    @Singleton
    fun provideQueryWords(): Query = FirebaseFirestore.getInstance()
        .collection("users/${FirebaseAuth.getInstance().currentUser?.email}/dictionary/")
        .limit(50)
}