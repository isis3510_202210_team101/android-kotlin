package com.moviles.langapp.model

data class AddWordRequestDao(
    val user: String,
    val latitude: Double,
    val longitude: Double,
    val learningLanguage: String,
    val word: String,
    val photo: String,
)