package com.moviles.langapp.model

data class Examples(
    var title: String,
    var content: String?,
    var author: String?,
    var ulr: String,
    var urlToImage: String
)
