package com.moviles.langapp.model

data class CardDetail(
    var word: String? = null,
    var description: String? = null,
    var urlToImage: String? = null
)