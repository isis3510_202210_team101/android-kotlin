package com.moviles.langapp.model

data class UserNearestDao(
    val user: String,
    val latitude: Double,
    val longitude: Double,
)
