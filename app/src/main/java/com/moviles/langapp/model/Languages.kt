package com.moviles.langapp.model

data class Languages(
    var language: String,
    var flag: String,
)