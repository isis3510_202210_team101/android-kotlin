package com.moviles.langapp.model

data class Words(
    var word: String = "",
    var thumbnail: String = "",
    var translation: String = "",
    var description: String = "",
    var urlToImage: String = ""
)