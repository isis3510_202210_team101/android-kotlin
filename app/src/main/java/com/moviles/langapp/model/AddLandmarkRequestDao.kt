package com.moviles.langapp.model

data class AddLandmarkRequestDao (
        val ImageUri: String,
        val user: String
)