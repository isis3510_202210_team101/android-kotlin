package com.moviles.langapp.model

data class Landmark(
    val description: String,
    val translatedName: String,
    val originalName: String,
    val photo: String,
)
