package com.moviles.langapp.model

data class WordResponseDao(
    val word: String
)
