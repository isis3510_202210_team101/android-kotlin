package com.moviles.langapp.model

data class Proportions(
    val English: Integer? = null,
    val German: Integer? = null,
    val Italian: Integer? = null,
    val Portuguese: Integer? = null,
    val Spanish: Integer? = null
)