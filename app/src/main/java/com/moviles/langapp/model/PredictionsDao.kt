package com.moviles.langapp.model

data class PredictionsDao(
    val native: String,
    val objective: String
)