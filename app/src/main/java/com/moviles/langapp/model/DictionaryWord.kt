package com.moviles.langapp.model

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.GeoPoint

data class DictionaryWord(
    var customTranslation: String = "",
    var location: GeoPoint? = null,
    var originalLanguage: DocumentReference? = null,
    var thumbnail: String = "",
    var word: DocumentReference? = null,
    var detailWord: DetailWord? = null
)
