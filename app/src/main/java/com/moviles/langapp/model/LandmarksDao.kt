package com.moviles.langapp.model

data class LandmarksDao (
    val englishName:String ,
    val score:String,
    val location_lat: Double,
    val location_long: Double,
    val description:String,
    val originalLanguage:String,
    val originalName:String
)