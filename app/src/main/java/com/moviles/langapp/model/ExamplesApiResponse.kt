package com.moviles.langapp.model

class ExamplesApiResponse {
    val status: String? = null
    val code: String? = null
    val articles: List<Examples>? = null
}
