package com.moviles.langapp.model

data class Card(
    var word: String = "",
    var translation: String = "",
    var thumbnail: String = ""
)