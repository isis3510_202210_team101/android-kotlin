package com.moviles.langapp.model

data class DetailWord(
    var description: String = "",
    var photo: String = "",
    var translation: String = ""

)
