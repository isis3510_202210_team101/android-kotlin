package com.moviles.langapp.provider

import com.moviles.langapp.model.PredictionsDao
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ObjectRecognitionProvider {

    @POST("/objectDetection")
    suspend fun getImageLabels(@Body body: okhttp3.RequestBody): Response<List<PredictionsDao>>
}