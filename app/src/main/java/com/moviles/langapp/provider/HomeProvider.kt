package com.moviles.langapp.provider

import com.moviles.langapp.model.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface HomeProvider {

    @POST("/nearest")
    suspend fun getNearestWords(@Body userNearestDao: UserNearestDao): Response<List<WordResponseDao>>

    @POST("/words")
    suspend fun addWord(@Body addWordRequestDao: AddWordRequestDao)

    @POST("/proportions")
    suspend fun langProportions(@Body user: UserProportions): Response<Proportions>

    @POST("/forgot")
    suspend fun getForgotten(@Body user: UserProportions): Response<List<String>>


}