package com.moviles.langapp.provider

import com.moviles.langapp.model.ExamplesApiResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


private const val API_KEY = "f37ca11fa096470ea9c8c2e8c889f725"

interface ExamplesProvider {

    @GET("top-headlines?apiKey=$API_KEY")
    suspend fun topHeadlines(@Query("country") country: String): Response<ExamplesApiResponse>

}