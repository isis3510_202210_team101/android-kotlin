package com.moviles.langapp.provider

import com.moviles.langapp.model.LandmarksDao
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import com.moviles.langapp.model.PredictionsDao
import com.moviles.langapp.model.UserNearestDao
import com.moviles.langapp.model.WordResponseDao
import retrofit2.http.Headers
import com.squareup.okhttp.RequestBody

interface LandmarkDetectionProvider {

    @POST("/landmarkDetection")
    suspend fun getLandmark(@Body body: okhttp3.RequestBody): Response<List<LandmarksDao>>
}