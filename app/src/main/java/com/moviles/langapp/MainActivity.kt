package com.moviles.langapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.compose.LangappTheme
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.ktx.Firebase
import com.moviles.langapp.repository.cache.NearestWordsDataStore
import com.moviles.langapp.utils.logEvent
import com.google.firebase.firestore.ktx.firestore
import com.moviles.langapp.utils.LangappContext
import com.moviles.langapp.utils.SharedPref
import com.moviles.langapp.viewmodels.DictionaryViewModel
import com.moviles.langapp.viewmodels.LoginScreenViewModel
import com.moviles.langapp.viewmodels.SignUpViewModel
import com.moviles.langapp.views.*
import com.squareup.okhttp.Dispatcher
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch


object Destinations {
    const val EXAMPLE_SCREEN = "EXAMPLE_SCREEN"
    const val LOGIN_SCREEN = "LOGIN_SCREEN"
    const val LOGIN_CREDENTIALS = "LOGIN_CREDENTIALS"
    const val HOME_SCREEN = "HOME_SCREEN"
    const val ONBOARD_SCREEN = "ONBOARD_SCREEN"
    const val ONBOARDNAT_SCREEN = "ONBOARDNAT_SCREEN"
    const val DICTIONARY_SCREEN = "DICTIONARY_SCREEN"
    const val SIGN_UP_1 = "SIGN_UP_1"
    const val ADD_NEW_WORD = "ADD_NEW_WORD"
    const val SELECT_DETECTED_WORDS = "SELECT_DETECTED_WORDS"
    const val ADD_DETECTED_WORD = "ADD_DETECTED_WORD"
    const val SELECT_LEARN_LANG = "SELECT_LEARN_LANG"
    const val ADD_LANDMARK_AND_WORDS = "ADD_LANDMARK_AND_WORDS"
    const val LANDMARKS_SCREEN = "LANDMARKS_SCREEN"
}

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var analytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        analytics = Firebase.analytics



        super.onCreate(savedInstanceState)
        LangappContext.setContext(this)
        setContent {

            LangappTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val navController = rememberNavController()
                    val firebaseAuth = FirebaseAuth.getInstance()
                    val localContext = LocalContext.current
                    val dataStoreNearest = NearestWordsDataStore(localContext)

                    val startDestination = if (firebaseAuth.currentUser != null) {
                        Destinations.HOME_SCREEN
                    } else {
                        Destinations.LOGIN_SCREEN
                    }
                    navController.addOnDestinationChangedListener { _, destination, _ ->
                        val params = Bundle()
                        params.putString(FirebaseAnalytics.Param.SCREEN_NAME, destination.route)
                        analytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, params)
                    }


                    NotificationMessage(viewModel = hiltViewModel())
                    NavHost(navController = navController, startDestination = startDestination) {

                        composable(Destinations.DICTIONARY_SCREEN) {
                            DictionaryScreen(navController, hiltViewModel())
                        }
                        composable(Destinations.SELECT_LEARN_LANG) {
                            SelectLearningLangScreen(navController)
                        }
                        composable(Destinations.EXAMPLE_SCREEN) {
                            ListScreen(navController = navController)
                        }
                        composable(Destinations.LOGIN_SCREEN) {
                            LoginScreen(navController)
                        }
                        composable(Destinations.LOGIN_CREDENTIALS) {
                            LoginCredentialsScreen(navController)
                        }
                        composable(Destinations.HOME_SCREEN) {
                            HomeScreenStart(navController, fusedLocationClient, dataStoreNearest)
                        }
                        composable(Destinations.SIGN_UP_1) {
                            SignUp1Screen(navController)
                        }
                        composable(Destinations.ONBOARD_SCREEN) {
                            OnboardingScreen(navController)
                        }
                        composable(Destinations.DICTIONARY_SCREEN) {
                            DictionaryScreen(navController, hiltViewModel())
                        }
                        composable(Destinations.ADD_NEW_WORD) {
                            AddWordScreen(navController, fusedLocationClient)
                        }
                        composable(Destinations.SELECT_DETECTED_WORDS) {
                            SelectDetectedWordsScreen(navController)
                        }
                        composable(Destinations.ADD_DETECTED_WORD) {
                            AddDetectedWordScreen(navController, fusedLocationClient)
                        }
                        composable(Destinations.ONBOARDNAT_SCREEN) {
                            OnboardingNatScreen(navController)
                        }
                        composable(Destinations.LANDMARKS_SCREEN){
                            LandmarkScreen(navController)
                        }
                        composable(Destinations.ADD_LANDMARK_AND_WORDS){
                            AddDetectedLanmarkAndWordsScreen(navController)
                        }
                    }
                }
            }
        }
    }
}
